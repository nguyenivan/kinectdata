﻿using KinectLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfHostedXna
{
    public class PoseMatchedEventArgs
    {
        public PoseType PoseType { get; private set; }

        public PoseMatchedEventArgs(PoseType poseType)
        {
            this.PoseType = poseType;
        }
    }
}
