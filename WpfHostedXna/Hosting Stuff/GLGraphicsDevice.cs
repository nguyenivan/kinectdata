﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows.Input;
using System.Windows;
using System.Runtime.InteropServices;
using Microsoft.Xna.Framework.Graphics;
using MGWinForms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using BetterSkinned;
using Primitives3D;
using AnimationAux;
using KinectLib;

namespace WpfHostedXna
{
    public class GLGraphicsDevice : GraphicsDeviceControl
    {
        private ContentManager contentManager;
        private AnimatedModel model = null;
        private CoordinateCross cross;
        private DateTime lastTick;
        private CubePrimitive cube;

        public GLGraphicsDevice()
        {
            // We must be notified of the application foreground status for our mouse input events
            Application.Current.Activated += new EventHandler(Current_Activated);
            Application.Current.Deactivated += new EventHandler(Current_Deactivated);
        }
        protected override void Initialize()
        {
            GraphicsDevice.PresentationParameters.BackBufferHeight = ClientSize.Height;
            GraphicsDevice.PresentationParameters.BackBufferWidth = ClientSize.Width;

            // Create our 3D cube object
            cube = new CubePrimitive(this.GraphicsDevice);
            cross = new CoordinateCross(this.GraphicsDevice, 100);

            this.contentManager = new ContentManager(this.Services, "Content");
            this.model = new AnimatedModel("dude");
            this.model.LoadContent(this.contentManager);

            //dance = new AnimatedModel("dude");
            //dance.LoadContent(this.contentManager);

            // Obtain the clip we want to play. I'm using an absolute index, 
            // because XNA 4.0 won't allow you to have more than one animation
            // associated with a model, anyway. It would be easy to add code
            // to look up the clip by name and to index it by name in the model.
            AnimationClip clip = model.Clips[0];

            // And play the clip
            AnimationPlayer player = model.PlayClip(clip);
            player.Looping = true;

            Sequence = new AnimationSequence(player, new List<AnimationStep> {
                new AnimationStep(PoseType.DungThang, 1.0f/30f),
                new AnimationStep(PoseType.LapTan, (float) clip.Duration/4f * 1f),
                new AnimationStep(PoseType.DungThang, (float) clip.Duration/4f * 2f),
                new AnimationStep(PoseType.VanVo, (float) clip.Duration/4f * 3f),
            });

            // Start the watch now that we're going to be starting our draw loop
            lastTick = DateTime.Now;

            // If we don't yet have a GraphicsDeviceService reference, we must add one for this control
            // Invoke the LoadContent event
            timer = new System.Timers.Timer(1000.0f / 60.0f);
            timer.Elapsed += OnRender;
            timer.Start();
        }
        protected override void Draw()
        {

            this.GraphicsDevice.Clear(Color.CornflowerBlue);

            int positionX = 150;
            int positionY = 150;
            int positionZ = -150;

            Matrix world = Matrix.Identity;
            Matrix view = Matrix.CreateLookAt(new Vector3(positionX, positionY, positionZ), new Vector3(0, 0, 0), Vector3.Up);

            Matrix projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, this.GraphicsDevice.Viewport.AspectRatio, 0.1f, 500.0f);

            // Get the color from our sliders
            Color color = Color.Blue;
            this.Sequence.Update(DateTime.Now - this.lastTick);
            //Draw a cube
            cube.Draw(world, view, projection, color);
            cross.Draw(world, view, projection);
            this.model.Draw(world, view, projection);

            this.lastTick = DateTime.Now;

        }

        // The GraphicsDeviceService that provides and manages our GraphicsDevice
        //public GraphicsDeviceService graphicsService;

        /// <summary>
        /// Invoked when the control has initialized the GraphicsDevice.
        /// </summary>
        private bool applicationHasFocus = false;
        private bool mouseInWindow = false;
        private HwndMouseState mouseState = new HwndMouseState();
        private bool isMouseCaptured = false;

        /// <summary>
        /// Invoked when the control receives a left mouse down message.
        /// </summary>
        public event EventHandler<HwndMouseEventArgs> HwndLButtonDown;

        /// <summary>
        /// Invoked when the control receives a left mouse up message.
        /// </summary>
        public event EventHandler<HwndMouseEventArgs> HwndLButtonUp;

        /// <summary>
        /// Invoked when the control receives a left mouse double click message.
        /// </summary>
        public event EventHandler<HwndMouseEventArgs> HwndLButtonDblClick;

        /// <summary>
        /// Invoked when the control receives a right mouse down message.
        /// </summary>
        public event EventHandler<HwndMouseEventArgs> HwndRButtonDown;

        /// <summary>
        /// Invoked when the control receives a right mouse up message.
        /// </summary>
        public event EventHandler<HwndMouseEventArgs> HwndRButtonUp;

        /// <summary>
        /// Invoked when the control receives a right mouse double click message.
        /// </summary>
        public event EventHandler<HwndMouseEventArgs> HwndRButtonDblClick;

        /// <summary>
        /// Invoked when the control receives a middle mouse down message.
        /// </summary>
        public event EventHandler<HwndMouseEventArgs> HwndMButtonDown;

        /// <summary>
        /// Invoked when the control receives a middle mouse up message.
        /// </summary>
        public event EventHandler<HwndMouseEventArgs> HwndMButtonUp;

        /// <summary>
        /// Invoked when the control receives a middle mouse double click message.
        /// </summary>
        public event EventHandler<HwndMouseEventArgs> HwndMButtonDblClick;

        /// <summary>
        /// Invoked when the control receives a mouse down message for the first extra button.
        /// </summary>
        public event EventHandler<HwndMouseEventArgs> HwndX1ButtonDown;

        /// <summary>
        /// Invoked when the control receives a mouse up message for the first extra button.
        /// </summary>
        public event EventHandler<HwndMouseEventArgs> HwndX1ButtonUp;

        /// <summary>
        /// Invoked when the control receives a double click message for the first extra mouse button.
        /// </summary>
        public event EventHandler<HwndMouseEventArgs> HwndX1ButtonDblClick;

        /// <summary>
        /// Invoked when the control receives a mouse down message for the second extra button.
        /// </summary>
        public event EventHandler<HwndMouseEventArgs> HwndX2ButtonDown;

        /// <summary>
        /// Invoked when the control receives a mouse up message for the second extra button.
        /// </summary>
        public event EventHandler<HwndMouseEventArgs> HwndX2ButtonUp;

        /// <summary>
        /// Invoked when the control receives a double click message for the first extra mouse button.
        /// </summary>
        public event EventHandler<HwndMouseEventArgs> HwndX2ButtonDblClick;

        /// <summary>
        /// Invoked when the control receives a mouse move message.
        /// </summary>
        public event EventHandler<HwndMouseEventArgs> HwndMouseMove;

        /// <summary>
        /// Invoked when the control first gets a mouse move message.
        /// </summary>
        public event EventHandler<HwndMouseEventArgs> HwndMouseEnter;

        /// <summary>
        /// Invoked when the control gets a mouse leave message.
        /// </summary>
        public event EventHandler<HwndMouseEventArgs> HwndMouseLeave;

        /// <summary>
        /// Invoked when the control gets a mouse wheel message.
        /// </summary>
        public event EventHandler<HwndMouseEventArgs> HwndMouseWheel;
        private Timer timer;

        //public event EventHandler<EventArgs> ControlInitialized;
        //public event EventHandler<EventArgs> ControlInitializing;

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (GraphicsDeviceService != null)
            {
                GraphicsDeviceService.Release();
            }

        }

        void Current_Activated(object sender, EventArgs e)
        {
            applicationHasFocus = true;
        }

        void Current_Deactivated(object sender, EventArgs e)
        {
            applicationHasFocus = false;
            ResetMouseState();

            if (mouseInWindow)
            {
                mouseInWindow = false;
                if (HwndMouseLeave != null)
                    HwndMouseLeave(this, new HwndMouseEventArgs(mouseState));
            }
        }

        private void ResetMouseState()
        {
            // We need to invoke events for any buttons that were pressed
            bool fireL = mouseState.LeftButton == MouseButtonState.Pressed;
            bool fireM = mouseState.MiddleButton == MouseButtonState.Pressed;
            bool fireR = mouseState.RightButton == MouseButtonState.Pressed;
            bool fireX1 = mouseState.X1Button == MouseButtonState.Pressed;
            bool fireX2 = mouseState.X2Button == MouseButtonState.Pressed;

            // Update the state of all of the buttons
            mouseState.LeftButton = MouseButtonState.Released;
            mouseState.MiddleButton = MouseButtonState.Released;
            mouseState.RightButton = MouseButtonState.Released;
            mouseState.X1Button = MouseButtonState.Released;
            mouseState.X2Button = MouseButtonState.Released;

            // Fire any events
            HwndMouseEventArgs args = new HwndMouseEventArgs(mouseState);
            if (fireL && HwndLButtonUp != null)
                HwndLButtonUp(this, args);
            if (fireM && HwndMButtonUp != null)
                HwndMButtonUp(this, args);
            if (fireR && HwndRButtonUp != null)
                HwndRButtonUp(this, args);
            if (fireX1 && HwndX1ButtonUp != null)
                HwndX1ButtonUp(this, args);
            if (fireX2 && HwndX2ButtonUp != null)
                HwndX2ButtonUp(this, args);
            // The mouse is no longer considered to be in our window
            mouseInWindow = false;
        }

        void OTKGLGraphicsDevice_SizeChanged(object sender, EventArgs e)
        {
            if (GraphicsDeviceService != null)
                GraphicsDeviceService.ResetDevice((int)Width, (int)Height);
        }

        private void OnRender(object sender, ElapsedEventArgs e)
        {
            if (applicationHasFocus)
                this.Invoke(new System.Windows.Forms.MethodInvoker(this.Invalidate));
        }


        protected override void WndProc(ref System.Windows.Forms.Message msg)
        {
            switch (msg.Msg)
            {
                case NativeMethods.WM_LBUTTONDOWN:
                    mouseState.LeftButton = MouseButtonState.Pressed;
                    if (HwndLButtonDown != null)
                        HwndLButtonDown(this, new HwndMouseEventArgs(mouseState));
                    break;
                case NativeMethods.WM_LBUTTONUP:
                    mouseState.LeftButton = MouseButtonState.Released;
                    if (HwndLButtonUp != null)
                        HwndLButtonUp(this, new HwndMouseEventArgs(mouseState));
                    break;
                case NativeMethods.WM_LBUTTONDBLCLK:
                    if (HwndLButtonDblClick != null)
                        HwndLButtonDblClick(this, new HwndMouseEventArgs(mouseState, MouseButton.Left));
                    break;
                case NativeMethods.WM_RBUTTONDOWN:
                    mouseState.RightButton = MouseButtonState.Pressed;
                    if (HwndRButtonDown != null)
                        HwndRButtonDown(this, new HwndMouseEventArgs(mouseState));
                    break;
                case NativeMethods.WM_RBUTTONUP:
                    mouseState.RightButton = MouseButtonState.Released;
                    if (HwndRButtonUp != null)
                        HwndRButtonUp(this, new HwndMouseEventArgs(mouseState));
                    break;
                case NativeMethods.WM_RBUTTONDBLCLK:
                    if (HwndRButtonDblClick != null)
                        HwndRButtonDblClick(this, new HwndMouseEventArgs(mouseState, MouseButton.Right));
                    break;
                case NativeMethods.WM_MBUTTONDOWN:
                    mouseState.MiddleButton = MouseButtonState.Pressed;
                    if (HwndMButtonDown != null)
                        HwndMButtonDown(this, new HwndMouseEventArgs(mouseState));
                    break;
                case NativeMethods.WM_MBUTTONUP:
                    mouseState.MiddleButton = MouseButtonState.Released;
                    if (HwndMButtonUp != null)
                        HwndMButtonUp(this, new HwndMouseEventArgs(mouseState));
                    break;
                case NativeMethods.WM_MBUTTONDBLCLK:
                    if (HwndMButtonDblClick != null)
                        HwndMButtonDblClick(this, new HwndMouseEventArgs(mouseState, MouseButton.Middle));
                    break;
                case NativeMethods.WM_XBUTTONDOWN:
                    if (((int)msg.WParam & NativeMethods.MK_XBUTTON1) != 0)
                    {
                        mouseState.X1Button = MouseButtonState.Pressed;
                        if (HwndX1ButtonDown != null)
                            HwndX1ButtonDown(this, new HwndMouseEventArgs(mouseState));
                    }
                    else if (((int)msg.WParam & NativeMethods.MK_XBUTTON2) != 0)
                    {
                        mouseState.X2Button = MouseButtonState.Pressed;
                        if (HwndX2ButtonDown != null)
                            HwndX2ButtonDown(this, new HwndMouseEventArgs(mouseState));
                    }
                    break;
                case NativeMethods.WM_XBUTTONUP:
                    if (((int)msg.WParam & NativeMethods.MK_XBUTTON1) != 0)
                    {
                        mouseState.X1Button = MouseButtonState.Released;
                        if (HwndX1ButtonUp != null)
                            HwndX1ButtonUp(this, new HwndMouseEventArgs(mouseState));
                    }
                    else if (((int)msg.WParam & NativeMethods.MK_XBUTTON2) != 0)
                    {
                        mouseState.X2Button = MouseButtonState.Released;
                        if (HwndX2ButtonUp != null)
                            HwndX2ButtonUp(this, new HwndMouseEventArgs(mouseState));
                    }
                    break;
                case NativeMethods.WM_XBUTTONDBLCLK:
                    if (((int)msg.WParam & NativeMethods.MK_XBUTTON1) != 0)
                    {
                        if (HwndX1ButtonDblClick != null)
                            HwndX1ButtonDblClick(this, new HwndMouseEventArgs(mouseState, MouseButton.XButton1));
                    }
                    else if (((int)msg.WParam & NativeMethods.MK_XBUTTON2) != 0)
                    {
                        if (HwndX2ButtonDblClick != null)
                            HwndX2ButtonDblClick(this, new HwndMouseEventArgs(mouseState, MouseButton.XButton2));
                    }
                    break;

                //case NativeMethods.WM_MOUSEHWHEEL:
                case NativeMethods.WM_MOUSEWHEEL:
                    // If the application isn't in focus, we don't handle this message
                    if (!applicationHasFocus || !mouseInWindow)
                        break;

                    int delta = 0;

                    unchecked
                    {
                        delta = NativeMethods.HighWord((int)msg.WParam) / NativeMethods.WHEEL_DELTA;
                    }

                    if (HwndMouseWheel != null && delta != 0)
                        HwndMouseWheel(this, new HwndMouseEventArgs(mouseState, delta, 0));

                    break;

                case NativeMethods.WM_MOUSEMOVE:
                    // If the application isn't in focus, we don't handle this message
                    if (!applicationHasFocus)
                        break;

                    // record the previous and new position of the mouse
                    mouseState.PreviousPosition = mouseState.Position;
                    mouseState.Position = new System.Windows.Point(
                        NativeMethods.GetXLParam((int)msg.LParam),
                        NativeMethods.GetYLParam((int)msg.LParam));

                    if (!mouseInWindow)
                    {
                        mouseInWindow = true;

                        // if the mouse is just entering, use the same position for the previous state
                        // so we don't get weird deltas happening when the move event fires
                        mouseState.PreviousPosition = mouseState.Position;

                        if (HwndMouseEnter != null)
                            HwndMouseEnter(this, new HwndMouseEventArgs(mouseState));

                        // send the track mouse event so that we get the WM_MOUSELEAVE message
                        NativeMethods.TRACKMOUSEEVENT tme = new NativeMethods.TRACKMOUSEEVENT();
                        tme.cbSize = Marshal.SizeOf(typeof(NativeMethods.TRACKMOUSEEVENT));
                        tme.dwFlags = NativeMethods.TME_LEAVE;
                        tme.hWnd = msg.HWnd;
                        NativeMethods.TrackMouseEvent(ref tme);
                    }

                    // Only fire the mouse move if the position actually changed
                    if (mouseState.Position != mouseState.PreviousPosition)
                    {
                        if (HwndMouseMove != null)
                            HwndMouseMove(this, new HwndMouseEventArgs(mouseState));
                    }

                    break;
                case NativeMethods.WM_MOUSELEAVE:

                    // If we have capture, we ignore this message because we're just
                    // going to reset the cursor position back into the window
                    if (isMouseCaptured)
                        break;

                    // Reset the state which releases all buttons and 
                    // marks the mouse as not being in the window.
                    ResetMouseState();

                    if (HwndMouseLeave != null)
                        HwndMouseLeave(this, new HwndMouseEventArgs(mouseState));
                    break;
                default:
                    break;
            }
            base.WndProc(ref msg);
        }

        public AnimationSequence Sequence { get; private set; }
    }
}
