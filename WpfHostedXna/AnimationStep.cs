﻿using KinectLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfHostedXna
{
    public class AnimationStep
    {
        public AnimationStep(PoseType poseType, float position)
        {
            PoseType = poseType;
            Position = position;
        }
        public PoseType PoseType
        {
            get;
            private set;
        }
        public float Position{
            get;
            private set;
        }
    }
}
