﻿#define FAKE_MATCH
#undef FAKE_MATCH
using KinectLib;
using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace WpfHostedXna
{
    class KinectProcessor : IDisposable, INotifyPropertyChanged
    {
        public ImageSource Canvas { get; set; }
        public event EventHandler<PoseMatchedEventArgs> PoseMatched;
        #region PRIVATE_MEMBER

        /// <summary>
        /// How many matched frames to be cached?
        /// </summary>
        private static readonly int MatchBufferSize = 10;

        /// <summary>
        /// How long should should the pose match (with the buffer)?
        /// </summary>
        private static readonly TimeSpan MatchTimeWindow = TimeSpan.FromMilliseconds(500);

        private readonly Dictionary<PoseType, ConcurrentQueue<DateTime>> OverflowQueues = new Dictionary<PoseType, ConcurrentQueue<DateTime>>
        {
            {PoseType.DungThang, new ConcurrentQueue<DateTime>()},
            {PoseType.VanVo, new ConcurrentQueue<DateTime>()},
            {PoseType.LapTan, new ConcurrentQueue<DateTime>()},
        };

        private const double HandSize = 30;

        /// <summary>
        /// Thickness of drawn joint lines
        /// </summary>
        private const double JointThickness = 3;

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as closed
        /// </summary>
        private readonly Brush handClosedBrush = new SolidColorBrush(Color.FromArgb(128, 255, 0, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as opened
        /// </summary>
        private readonly Brush handOpenBrush = new SolidColorBrush(Color.FromArgb(128, 0, 255, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as in lasso (pointer) position
        /// </summary>
        private readonly Brush handLassoBrush = new SolidColorBrush(Color.FromArgb(128, 0, 0, 255));

        /// <summary>
        /// Size of the RGB pixel in the bitmap
        /// </summary>
        private readonly int bytesPerPixel = (PixelFormats.Bgr32.BitsPerPixel + 7) / 8;

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor kinectSensor = null;

        /// <summary>
        /// Reader for body
        /// frames
        /// </summary>
        private ColorFrameReader colorFrameReader = null;

        /// <summary>
        /// Reader for body frames
        /// </summary>
        private BodyFrameReader bodyFrameReader = null;

        /// <summary>
        /// Width of display (depth space)
        /// </summary>
        private int displayWidth;

        /// <summary>
        /// Height of display (depth space)
        /// </summary>
        private int displayHeight;

        /// <summary>
        /// Array for the bodies
        /// </summary>
        private Body[] bodies = null;

        /// <summary>
        /// Drawing group for body rendering output
        /// </summary>
        private DrawingGroup drawingGroup;

        /// <summary>
        /// Drawing image that we will display
        /// </summary>
        private DrawingImage imageSource;

        /// <summary>
        /// Coordinate mapper to map one type of point to another
        /// </summary>
        private CoordinateMapper coordinateMapper = null;

        /// <summary>
        /// Pen used for drawing bones that are currently inferred
        /// </summary>        
        private readonly Pen inferredBonePen = new Pen(Brushes.Gray, 1);

        /// <summary>
        /// Pen used for drawing bones that are currently tracked
        /// </summary>
        private readonly Pen trackedBonePen = new Pen(Brushes.Green, 6);

        /// <summary>
        /// Brush used for drawing joints that are currently tracked
        /// </summary>
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));

        /// <summary>
        /// Brush used for drawing joints that are currently inferred
        /// </summary>        
        private readonly Brush inferredJointBrush = Brushes.Yellow;

        private WriteableBitmap realTimeImage = null;
        private DateTime startTime;

        #endregion

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource ImageSource
        {
            get
            {
                return this.imageSource;
            }
        }

        public KinectProcessor()
        {
            this.kinectSensor = KinectSensor.GetDefault();
            if (this.kinectSensor != null)
            {
                this.coordinateMapper = this.kinectSensor.CoordinateMapper;
                this.kinectSensor.Open();
                FrameDescription colorFrameDescription = this.kinectSensor.ColorFrameSource.FrameDescription;
                FrameDescription bodyFrameDescription = this.kinectSensor.DepthFrameSource.FrameDescription;
                this.displayWidth = colorFrameDescription.Width;
                this.displayHeight = colorFrameDescription.Height;
                this.bodies = new Body[this.kinectSensor.BodyFrameSource.BodyCount];
                this.colorFrameReader = this.kinectSensor.ColorFrameSource.OpenReader();
                this.bodyFrameReader = this.kinectSensor.BodyFrameSource.OpenReader();
                this.drawingGroup = new DrawingGroup();
                this.imageSource = new DrawingImage(this.drawingGroup);

                if (this.colorFrameReader != null)
                {
                    this.colorFrameReader.FrameArrived += this.ColorFrameReaderFrameArrived;
                }

                if (this.bodyFrameReader != null)
                {
                    this.bodyFrameReader.FrameArrived += this.BodyFrameReaderFrameArrived;
                }
            }
#if FAKE_MATCH
            var timer = new System.Timers.Timer(2000f);
            timer.Elapsed += Elapse;
            startTime = DateTime.Now;
            timer.Start(); 
#endif
        }

        PoseType[] sequence = new PoseType[]{
            PoseType.DungThang, 
            PoseType.LapTan, 
            PoseType.DungThang,
            PoseType.VanVo
        };

        private void Elapse(object sender, System.Timers.ElapsedEventArgs e)
        {
            PoseType pose = sequence[((e.SignalTime - startTime).Seconds / 2 - 1) % 4];
            PoseMatched(this, new PoseMatchedEventArgs(pose));
        }

        private void ColorFrameReaderFrameArrived(object sender, ColorFrameArrivedEventArgs e)
        {
            ColorFrameReference frameReference = e.FrameReference;
            try
            {
                ColorFrame frame = frameReference.AcquireFrame();

                if (frame != null)
                {
                    // ColorFrame is IDisposable
                    using (frame)
                    {
                        FrameDescription frameDescription = frame.CreateFrameDescription(ColorImageFormat.Bgra);
                        int width = frameDescription.Width;
                        int height = frameDescription.Height;
                        uint bufferLength = (uint)(frameDescription.LengthInPixels * frameDescription.BytesPerPixel);
                        if (null == realTimeImage)
                        {
                            realTimeImage = new WriteableBitmap(width, height, 96, 96, PixelFormats.Bgra32, null);
                        }
                        realTimeImage.Lock();
                        frame.CopyConvertedFrameDataToIntPtr(realTimeImage.BackBuffer,bufferLength, ColorImageFormat.Bgra);
                        realTimeImage.AddDirtyRect(new Int32Rect(0, 0, width, height));
                        realTimeImage.Unlock();
                        NotifyPropertyChanged("ImageSource");
                    }
                }
            }
            catch (Exception)
            {
                throw;
                // ignore if the frame is no longer available
            }
        }

        private void BodyFrameReaderFrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            BodyFrameReference frameReference = e.FrameReference;
            // If nothing is tracked, null the joints reference
            try
            {
                BodyFrame frame = frameReference.AcquireFrame();

                if (frame != null)
                {
                    // BodyFrame is IDisposable
                    using (frame)
                    {
                        using (DrawingContext dc = this.drawingGroup.Open())
                        {
                            // Draw a transparent background to set the render size
                            dc.DrawRectangle(Brushes.Transparent, null, new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));
                            DrawColorOnCanvas(dc);
                            // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                            // As long as those body objects are not disposed and not set to null in the array,
                            // those body objects will be re-used.
                            frame.GetAndRefreshBodyData(this.bodies);
                            foreach (Body body in this.bodies)
                            {
                                if (body.IsTracked)
                                {
                                    if (null != body.Joints)
                                    {
                                        // Create double array from joint set, we need to create a deep copy first
                                        var simpleBody = new SimpleBody()
                                        {
                                            Joints = body.Joints.ToDictionary( entry => entry.Key, entry => entry.Value),
                                            JointOrientations = body.JointOrientations.ToDictionary(entry => entry.Key, entry => entry.Value),
                                            FloorClipPlane = frame.FloorClipPlane
                                        };

                                        PoseType pose = MatchPose(simpleBody);
                                        if (pose != PoseType.Unknown)
                                        {
                                            PoseMatched(this, new PoseMatchedEventArgs(pose));
                                        }

                                        DrawBodyOnCanvas(simpleBody, body.HandLeftState, body.HandRightState, dc);
                                    }
                                }
                            }
                            // prevent drawing outside of our render area
                            this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void DrawColorOnCanvas(DrawingContext dc)
        {
            // draw the kinect bitmap if it's there
            if (null != this.realTimeImage)
            {
                // determine the coordinates for displaying the image
                Double w = realTimeImage.Width * this.displayHeight / realTimeImage.Height;
                Double diffWidth = Math.Abs(this.displayWidth - w);
                Double x = -(diffWidth / 2);
                Double ww = w + x;
                dc.DrawImage(realTimeImage, new Rect(x, 0.0, w, this.displayHeight));
            }
        }

        private void DrawBodyOnCanvas(SimpleBody simpleBody, HandState handLeftState, HandState handRightState, DrawingContext dc)
        {
            // convert the joint points to depth (display) space
            Dictionary<JointType, Point> jointPoints = new Dictionary<JointType, Point>();
            var joints = simpleBody.Joints;
            foreach (JointType jointType in joints.Keys)
            {
                ColorSpacePoint colorSpacePoint = this.coordinateMapper.MapCameraPointToColorSpace(joints[jointType].Position);
                jointPoints[jointType] = new Point(colorSpacePoint.X, colorSpacePoint.Y);
            }
            this.DrawBody(joints, jointPoints, dc);
            this.DrawHand(handLeftState, jointPoints[JointType.HandLeft], dc);
            this.DrawHand(handRightState, jointPoints[JointType.HandRight], dc);
        }

        private PoseType MatchPose(SimpleBody simpleBody)
        {
            /// TODO: need to replace with body frame arrival time
            var now = DateTime.Now;
            for (PoseType poseType = PoseType.VanVo; poseType<PoseType.Unknown; poseType++)
            {
                ///TODO: match better pose
                //var y = PoseMatcher.Match(simpleBody, poseType);
                //if (y != 1) continue;
                var queue = this.OverflowQueues[poseType];
                var queueFull = queue.EnqueueOverflow(DateTime.Now, MatchBufferSize);
                if (!queueFull) continue;
                if (queue.Last() - now > MatchTimeWindow) continue;
                queue.Clear();
                return poseType;
            }
            return PoseType.Unknown;
        }

        #region Canvas Drawing Methods
        /// <summary>
        /// Draws a hand symbol if the hand is tracked: red circle = closed, green circle = opened; blue circle = lasso
        /// </summary>
        /// <param name="handState">state of the hand</param>
        /// <param name="handPosition">position of the hand</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawHand(HandState handState, Point handPosition, DrawingContext drawingContext)
        {
            switch (handState)
            {
                case HandState.Closed:
                    drawingContext.DrawEllipse(this.handClosedBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Open:
                    drawingContext.DrawEllipse(this.handOpenBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Lasso:
                    drawingContext.DrawEllipse(this.handLassoBrush, null, handPosition, HandSize, HandSize);


                    break;
            }
        }

        /// <summary>
        /// Draws one bone of a body (joint to joint)
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="jointType0">first joint of bone to draw</param>
        /// <param name="jointType1">second joint of bone to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawBone(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, Point> jointPoints, JointType jointType0, JointType jointType1, DrawingContext drawingContext)
        {
            Joint joint0 = joints[jointType0];
            Joint joint1 = joints[jointType1];

            // If we can't find either of these joints, exit
            if (joint0.TrackingState == TrackingState.NotTracked ||
                joint1.TrackingState == TrackingState.NotTracked)
            {
                return;
            }

            // Don't draw if both points are inferred
            if (joint0.TrackingState == TrackingState.Inferred &&
                joint1.TrackingState == TrackingState.Inferred)
            {
                return;
            }

            // We assume all drawn bones are inferred unless BOTH joints are tracked
            Pen drawPen = this.inferredBonePen;
            if ((joint0.TrackingState == TrackingState.Tracked) && (joint1.TrackingState == TrackingState.Tracked))
            {
                drawPen = this.trackedBonePen;
            }

            drawingContext.DrawLine(drawPen, jointPoints[jointType0], jointPoints[jointType1]);
        }

        /// <summary>
        /// Draws a body
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawBody(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, Point> jointPoints, DrawingContext drawingContext)
        {
            // Draw the bones

            // Torso
            this.DrawBone(joints, jointPoints, JointType.Head, JointType.Neck, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.Neck, JointType.SpineShoulder, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.SpineMid, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineMid, JointType.SpineBase, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.ShoulderRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.ShoulderLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineBase, JointType.HipRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineBase, JointType.HipLeft, drawingContext);

            // Right Arm    
            this.DrawBone(joints, jointPoints, JointType.ShoulderRight, JointType.ElbowRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.ElbowRight, JointType.WristRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristRight, JointType.HandRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.HandRight, JointType.HandTipRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristRight, JointType.ThumbRight, drawingContext);

            // Left Arm
            this.DrawBone(joints, jointPoints, JointType.ShoulderLeft, JointType.ElbowLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.ElbowLeft, JointType.WristLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristLeft, JointType.HandLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.HandLeft, JointType.HandTipLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristLeft, JointType.ThumbLeft, drawingContext);

            // Right Leg
            this.DrawBone(joints, jointPoints, JointType.HipRight, JointType.KneeRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.KneeRight, JointType.AnkleRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.AnkleRight, JointType.FootRight, drawingContext);

            // Left Leg
            this.DrawBone(joints, jointPoints, JointType.HipLeft, JointType.KneeLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.KneeLeft, JointType.AnkleLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.AnkleLeft, JointType.FootLeft, drawingContext);

            // Draw the joints
            foreach (JointType jointType in joints.Keys)
            {
                Brush drawBrush = null;

                TrackingState trackingState = joints[jointType].TrackingState;

                if (trackingState == TrackingState.Tracked)
                {
                    drawBrush = this.trackedJointBrush;
                }
                else if (trackingState == TrackingState.Inferred)
                {
                    drawBrush = this.inferredJointBrush;
                }

                if (drawBrush != null)
                {
                    drawingContext.DrawEllipse(drawBrush, null, jointPoints[jointType], JointThickness, JointThickness);
                }
            }
        }
        #endregion

        public void Dispose()
        {
            if (this.colorFrameReader != null)
            {
                // ColorFrameReader is IDisposable
                this.colorFrameReader.Dispose();
                this.colorFrameReader = null;

                // BodyFrameReader is IDisposable
                this.bodyFrameReader.Dispose();
                this.bodyFrameReader = null;
            }

            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        } 

        #endregion
    }
}
