﻿#region File Description
//-----------------------------------------------------------------------------
// MainWindow.xaml.cs
//
// Copyright 2011, Nick Gravelyn.
// Licensed under the terms of the Ms-PL: http://www.microsoft.com/opensource/licenses.mspx#Ms-PL
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Diagnostics;
using System.Windows;
using Microsoft.Xna.Framework;
using Primitives3D;
using System.Windows.Forms;
using Microsoft.Xna.Framework.Content;
using BetterSkinned;
using Microsoft.Xna.Framework.Graphics;
using ServiceContainer = System.ComponentModel.Design.ServiceContainer;
using MGWinForms;
using AnimationAux;
using System.Windows.Media.Imaging;
using System.Linq;
using System.Windows.Controls;
using System.Reflection;
using System.Windows.Media.Animation;
using System.Windows.Media;
using KinectLib;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Microsoft.Kinect;

namespace WpfHostedXna
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private GLGraphicsDevice graphicsDevice;
        private KinectProcessor processor;

        /// <summary>
        /// Initiate bitmaps for red and green leds
        /// </summary>
        private readonly BitmapImage greenLED = new BitmapImage(new Uri(@"/KinectLib;component/Resources/greenled.png", UriKind.Relative));
        private readonly BitmapImage redLED = new BitmapImage(new Uri(@"/KinectLib;component/Resources/redled.png", UriKind.Relative));
        private Image[] leds = null;

        private readonly Storyboard ledStoryBoard = new Storyboard();

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource ImageSource
        {
            get
            {
                return this.processor.ImageSource;
            }
        }



        public MainWindow()
        {
            //this.kinectSensor = KinectSensor.Default;
            //if (this.kinectSensor != null)
            //{
            //    this.coordinateMapper = this.kinectSensor.CoordinateMapper;
            //    this.kinectSensor.Open();
            //    FrameDescription colorFrameDescription = this.kinectSensor.ColorFrameSource.FrameDescription;
            //    FrameDescription bodyFrameDescription = this.kinectSensor.DepthFrameSource.FrameDescription;
            //    this.displayWidth = colorFrameDescription.Width;
            //    this.displayHeight = colorFrameDescription.Height;
            //    this.bodies = new Body[this.kinectSensor.BodyFrameSource.BodyCount];
            //    this.colorFrameReader = this.kinectSensor.ColorFrameSource.OpenReader();
            //    this.bodyFrameReader = this.kinectSensor.BodyFrameSource.OpenReader();
            //    this.drawingGroup = new DrawingGroup();
            //    this.imageSource = new DrawingImage(this.drawingGroup);

            //}
            this.processor = new KinectProcessor();
            InitializeComponent();
            this.DataContext = this;

            this.graphicsDevice = new GLGraphicsDevice();
            this.OglForm.Child = this.graphicsDevice;

            // Setup the LEDs
            this.leds = (from c in this.LEDBoard.Children.OfType<Image>()
                         where "LED".Equals(c.Tag)
                         select c).ToArray<Image>();
            foreach (Image l in leds)
            {
                l.Source = redLED;
            }
            DoubleAnimation quickFlash = new DoubleAnimation()
            {
                From = 1.0,
                To = 0.0,
                Duration = new Duration(TimeSpan.FromMilliseconds(200)),
                AutoReverse = true
            };
            Storyboard.SetTargetName(quickFlash, "LEDBoard");
            Storyboard.SetTargetProperty(quickFlash, new PropertyPath(StackPanel.OpacityProperty));
            ledStoryBoard.Children.Add(quickFlash);
        }

        //private void BodyFrameReaderFrameArrived(object sender, BodyFrameArrivedEventArgs e)
        //{
        //    return;
        //}

        //private void ColorFrameReaderFrameArrived(object sender, ColorFrameArrivedEventArgs e)
        //{
        //    var frame = e.FrameReference.AcquireFrame();
        //    return;
        //}

        void processor_PoseMatched(object sender, PoseMatchedEventArgs e)
        {
            Debug.WriteLine("Pose match: %s", e.PoseType);
            FlashAllLeds();
        }

        private void FlashAllLeds()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                ledStoryBoard.Begin(this, true);
                Extensions.Beep();
            }));
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (null != this.processor)
            {
                this.processor.Dispose();
                this.processor = null;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //if (this.colorFrameReader != null)
            //{
            //    this.colorFrameReader.FrameArrived += this.ColorFrameReaderFrameArrived;
            //}

            //if (this.bodyFrameReader != null)
            //{
            //    this.bodyFrameReader.FrameArrived += this.BodyFrameReaderFrameArrived;
            //}
            this.processor.PoseMatched += processor_PoseMatched;
            this.processor.PoseMatched += this.graphicsDevice.Sequence.PoseMatchedEventListener;
            this.processor.PropertyChanged += processor_PropertyChanged;
        }

        void processor_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            NotifyPropertyChanged();
        }


        #region IPropertyChange Members

        public event PropertyChangedEventHandler PropertyChanged;
        //private KinectSensor kinectSensor;
        //private CoordinateMapper coordinateMapper;
        //private int displayWidth;
        //private int displayHeight;
        //private Body[] bodies;
        //private ColorFrameReader colorFrameReader;
        //private BodyFrameReader bodyFrameReader;
        //private DrawingGroup drawingGroup;
        //private DrawingImage imageSource;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        } 

        #endregion

    }
}
