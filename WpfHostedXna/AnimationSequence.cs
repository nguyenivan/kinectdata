﻿using BetterSkinned;
using KinectLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfHostedXna
{
    public class AnimationSequence
    {
        private int currentIndex = 0;

        public AnimationPlayer AnimationPlayer { get; set; }

        public List<AnimationStep> Sequence
        {
            get;
            set;
        }

        public AnimationSequence(AnimationPlayer player):this(player, new List<AnimationStep>())
        {
        }

        public AnimationSequence(AnimationPlayer player, List<AnimationStep> initSequence)
        {
            this.Sequence = initSequence;
            this.Procceed = false;
            this.AnimationPlayer = player;
            this.AnimationPlayer.Position = CurrentStep.Position;
        }

        public void PoseMatchedEventListener(object sender, PoseMatchedEventArgs e)
        {
            this.Procceed = true;
            if (e.PoseType == this.CurrentStep.PoseType)
            {
                this.Procceed = true;
            }
        }


        public AnimationStep CurrentStep {
            get
            {
                if (this.Sequence.Count == 0) return null;
                return this.Sequence[currentIndex % this.Sequence.Count];
            }
        }

        public AnimationStep NextStep
        {
            get
            {
                if (this.Sequence.Count == 0) return null;
                return this.Sequence[(currentIndex + 1) % this.Sequence.Count];
            }
        }

        public void Forward()
        {
            if (this.Sequence.Count == 0) return;
            this.Procceed = false;
            currentIndex = (currentIndex + 1) % this.Sequence.Count;
            this.AnimationPlayer.Position = CurrentStep.Position;
        }

        public void Back()
        {
            if (this.Sequence.Count == 0) return;
            this.Procceed = false;
            currentIndex = (currentIndex + this.Sequence.Count - 1) % this.Sequence.Count;
            this.AnimationPlayer.Position = CurrentStep.Position;
        }

        public void Update(TimeSpan timeSpan)
        {
            if (this.Sequence.Count == 0) return;
            if (!this.Procceed) return;
            float position = this.AnimationPlayer.Position + (float)timeSpan.TotalMilliseconds / 1000f;
            if (CurrentStep.Position > NextStep.Position)
            {
                // Looping ?
                if (position > this.AnimationPlayer.Duration)
                {
                    position -= this.AnimationPlayer.Duration;
                    
                }
            }
            if (position < NextStep.Position)
            {
                this.AnimationPlayer.Position = position;
            }
            else
            {
                this.Forward();
            }
        }

        public bool Procceed { get; set; }
    }
}
