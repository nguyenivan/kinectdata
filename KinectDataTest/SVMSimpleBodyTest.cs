﻿using KinectLib;
using KinectLib.Biovision;
using Microsoft.Kinect;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenTK;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Windows.Resources;
using System.Xml;
using System.Xml.Schema;
using C3D;
using System.Text;
using System.Resources;
using System.Threading;
using System.Windows.Media.Imaging;
using System.Drawing;

namespace KinectDataTest
{
    [TestClass]
    public class SVMSimpleBodyTest
    {


        [TestMethod]
        public void OneTimeTest()
        {
            var bmp = (Bitmap)Properties.Resources.ResourceManager.GetObject("DungThang");
            var b = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
               bmp.GetHbitmap(),
               IntPtr.Zero,
               System.Windows.Int32Rect.Empty,
               BitmapSizeOptions.FromEmptyOptions());
            Console.WriteLine("{0}x{1}", b.PixelWidth, b.PixelHeight);
            Console.WriteLine("{0}x{1}", b.Width, b.Height);
            bmp = (Bitmap)Properties.Resources.ResourceManager.GetObject("ABCD");
            Assert.IsNull(bmp);
        }

        [TestMethod]
        public void MatchPoseWithMovementTake01()
        {
            var serializer = new DataContractSerializer(typeof(List<SimpleBody>));
            var path = @"C:\Users\Nguyen\Projects\KinectData\Data\201408-TAKE01-Ordered\FORM01-Movements-TAKE02-20140811";
            var matcher = new PoseMatcher();
            var form = new FormTraining(PowerForm.FormOne);

            form.OnStart += delegate { Console.WriteLine("Start"); };
            form.OnPause += delegate { Console.WriteLine("Pause"); };
            form.OnFinish += delegate { Console.WriteLine("Finish"); };
            form.OnMatch += delegate(System.Object o, PoseEventArgs e)
            { Console.WriteLine(e.Pose.GetName()); };
            form.OnProceed += delegate(System.Object o, PoseEventArgs e)
            { Console.WriteLine("Proceeding at pose {0}", e.Pose.GetName()); };
            form.Start();
            foreach (var f in Directory.GetFiles(path, "*.xml"))
            {
                using (var ms = new FileStream(f, FileMode.Open))
                {
                    var bodyList = (List<SimpleBody>)serializer.ReadObject(ms);
                    foreach (var body in bodyList)
                    {
                        form.AddBody(body);

                        //var match = matcher.Match(body);
                        //Console.WriteLine(match);
                    }
                }
            }

        }

        [TestMethod]
        public void MatchPoseWithFullSet()
        {
            var serializer = new DataContractSerializer(typeof(List<SimpleBody>));
            var path = @"C:\Users\Nguyen\Projects\KinectData\Data\201408-TAKE01-Ordered";
            var matcher = new PoseMatcher();
            for (PoseType pose = PoseType.Unknown; pose <= PoseType.QuaiChuyTrai; pose++)
            {
                var dirPattern = string.Format("{0:00}*", (int)pose);
                foreach (var dir in Directory.GetDirectories(path, dirPattern))
                {
                    foreach (var f in Directory.GetFiles(dir, "*.xml"))
                    {
                        using (var ms = new FileStream(f, FileMode.Open))
                        {
                            var bodyList = (List<SimpleBody>)serializer.ReadObject(ms);
                            foreach (var body in bodyList)
                            {
                                var match = matcher.Match(body);
                                Console.WriteLine(match);
                            }
                        }
                    }
                }
            }
        }

        [TestMethod]
        public void GenerateTake01Data()
        {
            var serializer = new DataContractSerializer(typeof(List<SimpleBody>));
            var path = @"C:\Users\Nguyen\Projects\KinectData\Data\201408-TAKE01-Ordered";
            var outName = @"out.txt";
            var sb = new StringBuilder();
            //var template = "{idxW}:{W} {idxX}:{X} {idxY}:{Y} {idxZ}:{Z}";
            using (StreamWriter sw = new StreamWriter(
                System.IO.Path.Combine(path, outName), false, System.Text.Encoding.ASCII))
            {
                for (PoseType pose = PoseType.Unknown; pose <= PoseType.QuaiChuyTrai; pose++)
                {
                    var dirPattern = string.Format("{0:00}*", (int)pose);
                    foreach (var dir in Directory.GetDirectories(path, dirPattern))
                    {
                        foreach (var f in Directory.GetFiles(dir, "*.xml"))
                        {
                            using (var ms = new FileStream(f, FileMode.Open))
                            {
                                var bodyList = (List<SimpleBody>)serializer.ReadObject(ms);
                                foreach (var body in bodyList)
                                {
                                    sb.AppendFormat("{0} ", (int)pose);
                                    var localRotations = body.GetLocalOrientations();
                                    int i = 0;
                                    foreach (var joint in SimpleBody.selectedJoints)
                                    {
                                        if (localRotations.ContainsKey(joint))
                                        {

                                            sb.AppendFormat("{0}:{1} {2}:{3} {4}:{5} {6}:{7}",
                                                i * 4 + 1,
                                                localRotations[joint].W,
                                                i * 4 + 2,
                                                localRotations[joint].X,
                                                i * 4 + 3,
                                                localRotations[joint].Y,
                                                i * 4 + 4,
                                                localRotations[joint].Z
                                            );
                                        }
                                        i++;
                                        sb.Append(i < SimpleBody.selectedJoints.Count() ? " " : string.Empty);
                                    }
                                    sw.WriteLine(sb.ToString());
                                    sb.Clear();
                                }

                            }
                        }
                    }
                }
            }
        }

        [TestMethod]
        public void C3DFORM01()
        {
            var SCALE = 100f;
            var PRECISION = 0.1f;
            Int16 START_AT_FRAME = 5;
            var pathhList = new List<string>()
            {
                @"C:\Users\Nguyen\Projects\KinectData\Data\201408-TAKE01-Ordered\FORM01-Movements-TAKE01-20140811\1bodies332.xml",
                @"C:\Users\Nguyen\Projects\KinectData\Data\201408-TAKE01-Ordered\FORM01-Movements-TAKE01-20140811\1bodies318.xml",
                @"C:\Users\Nguyen\Projects\KinectData\Data\201408-TAKE01-Ordered\FORM01-Movements-TAKE01-20140811\1bodies309.xml",
                @"C:\Users\Nguyen\Projects\KinectData\Data\201408-TAKE01-Ordered\FORM01-Movements-TAKE01-20140811\1bodies307.xml",
                @"C:\Users\Nguyen\Projects\KinectData\Data\201408-TAKE01-Ordered\FORM01-Movements-TAKE01-20140811\1bodies308.xml"
            };
            var output = @"C:\Users\Nguyen\Desktop\nguyen1.c3d";
            var jointTypes = Enum.GetValues(typeof(JointType));

            var jointCount = (Int16)jointTypes.Length;
            var serializer = new DataContractSerializer(typeof(List<SimpleBody>));
            C3D.C3DFile file = C3D.C3DFile.Create();
            file.Parameters.SetGroup(1, "TRIAL", "");
            file.Parameters[1].Add("ACTUAL_START_FIELD", "").SetData<Int16[]>(new Int16[] { START_AT_FRAME, 0 });
            file.Parameters.SetGroup(2, "POINT", "");
            file.Parameters[2].Add("USED", "").SetData<Int16>(jointCount);
            file.Parameters[2].Add("UNITS", "").SetData<String>("cm");
            file.Parameters[2].Add("X_SCREEN", "").SetData<String>("-X");
            file.Parameters[2].Add("Y_SCREEN", "").SetData<String>("+Y");
            file.Parameters[2].Add("SCALE", "").SetData<Single>(PRECISION);
            var labels = jointTypes.Cast<JointType>().Select(x => x.ToString()).ToArray();
            file.Parameters[2].Add("LABELS", "").SetData<String[]>(labels);

            var bodyCount = 0;

            OpenTK.Matrix4 floorMatrix = new Matrix4();

            foreach (var path in pathhList)
            {
                using (var ms = new FileStream(path, FileMode.Open))
                {
                    var testBodyList = (List<SimpleBody>)serializer.ReadObject(ms);

                    if (bodyCount == 0)
                    {
                        if (START_AT_FRAME > testBodyList.Count)
                        {
                            throw new Exception("Cannot start beyond first set of body list");
                        }
                        // Calculate the transform matrix for Floor Plane

                        var floor = testBodyList[START_AT_FRAME].FloorClipPlane;

                        var floorNormal = new Vector3() { X = floor.X, Y = floor.Y, Z = floor.Z };
                        var floorDistance =
                            (testBodyList[START_AT_FRAME].Joints[Microsoft.Kinect.JointType.FootLeft].Position.Y +
                            testBodyList[START_AT_FRAME].Joints[Microsoft.Kinect.JointType.FootRight].Position.Y) / 2;

                        floorMatrix = Extensions.CameraTransform(floorNormal, -floorDistance);
                    }

                    foreach (var body in testBodyList)
                    {
                        bodyCount++;

                        var data = body.Joints.OrderBy(x => x.Value.JointType).Select(x =>
                        {
                            var v = new Vector3()
                            {
                                X = x.Value.Position.X,
                                Y = x.Value.Position.Y,
                                Z = x.Value.Position.Z
                            };
                            v = Vector3.Transform(v, floorMatrix);
                            return new C3DPoint3DData()
                            {
                                X = v.X * SCALE,
                                Y = v.Y * SCALE,
                                Z = v.Z * SCALE,
                                Residual = 1.0f,
                                CameraMask = 1
                            };
                        }).ToArray();
                        //var data = body.Joints.OrderBy( x => x.Value.JointType).Select(x => new C3DPoint3DData()
                        //{
                        //    X = -x.Value.Position.X * SCALE,
                        //    Y = x.Value.Position.Y * SCALE,
                        //    Z = -x.Value.Position.Z * SCALE,
                        //    Residual = 1.0f,
                        //    CameraMask = 1
                        //}).ToArray();
                        file.AllFrames.Add(new C3DFrame(data));
                    }
                }
            }
            file.Header.PointCount = (ushort)jointCount;
            file.Header.FirstFrameIndex = 1;
            file.Header.LastFrameIndex = (ushort)bodyCount;
            file.Header.FrameRate = 30;
            file.Header.AnalogMeasurementCount = 0;
            file.Header.AnalogSamplesPerFrame = 1;
            file.Header.MaxInterpolationGaps = 0;
            file.Header.HasLableRangeData = false;
            file.Header.IsSupport4CharsLabel = false;
            file.Header.ScaleFactor = PRECISION;
            file.Header.FirstFrameIndex = (ushort)START_AT_FRAME;
            file.SaveTo(output);
        }



        [TestMethod]
        public void EulerAnglesOfChildPrime()
        {
            var qP = new Quaternion() { W = 0.7305f, X = -0.6263f, Y = -0.0205f, Z = -0.2717f };
            var qC = new Quaternion() { W = 0.4705f, X = 0.4187f, Y = 0.5094f, Z = -0.5864f };

            var qPprime = new Quaternion() { W = -0.0029f, X = 0.3102f, Y = -0.6748f, Z = -0.6697f };
            var qCprime = new Quaternion() { W = -0.2192f, X = 0.9195f, Y = -0.3192f, Z = 0.0677f };

            var qDiff = Extensions.DiffQuaterion(qP, qC, qPprime, qCprime);

            Console.WriteLine(qDiff.MeaningfulDisplay());

            var euler = EulerRotation.FromQ(qDiff);
            /// Discovery: Yaw: X, Pitch: Y, Roll: Z
            /// When rotate, the order must be X, Y, Z
            Console.WriteLine(euler);

        }

        [ClassInitialize]
        public static void Setup(TestContext context)
        {
            string s = System.IO.Packaging.PackUriHelper.UriSchemePack;
        }
    }
}
