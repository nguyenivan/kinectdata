﻿using KinectLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PoseMatcher
{
    /// <summary>
    /// Interaction logic for ConfirmDialog.xaml
    /// </summary>
    public partial class ConfirmDialog : Window
    {

        VoiceCommand defaultAnswer;
        public ConfirmDialog(string message, VoiceCommand defaultAnswer = VoiceCommand.No)
        {
            InitializeComponent();
            this.Message.Text = message;
            this.defaultAnswer = defaultAnswer;
            this.ActiveCommand = VoiceCommand.No;
            IsShowingDialog = false;
        }

        public VoiceCommand Answer
        {
            get;
            internal set;
        }

        private void Yes_Click(object sender, RoutedEventArgs e)
        {
            this.Answer = VoiceCommand.Yes;
            this.Close();
        }

        private void No_Click(object sender, RoutedEventArgs e)
        {
            this.Answer = VoiceCommand.No;
            this.Close();
        }

        public void Show(FormTrainingState currentState, VoiceCommand voiceCommand)
        {
            this.Answer = VoiceCommand.No;
            this.CurrentState = currentState;
            this.ActiveCommand = voiceCommand;
            this.IsShowingDialog = true;
            this.Owner.IsEnabled = false;
            this.Show();
        }

        public bool IsShowingDialog { get; set; }

        public VoiceCommand ActiveCommand { get; set; }

        public FormTrainingState CurrentState { get; set; }
    }
}
