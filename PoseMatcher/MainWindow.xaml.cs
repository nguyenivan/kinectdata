﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Kinect;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Threading;
using System.Windows.Media.Animation;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Drawing.Imaging;
using System.Windows.Media.Media3D;
using System.Threading;
using System.Runtime.Serialization;
using System.Xml;
using System.Text;
using KinectLib;
using Timer = System.Timers.Timer;
using System.Timers;
using System.Windows.Documents;
using System.Windows.Interop;

namespace PoseMatcher
{
    /// <summary>
    /// Tracking state
    /// </summary>
    public enum TrackState { Idle, Preparing, Tracking };

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region Private members
        /// <summary>
        /// Radius of drawn hand circles
        /// </summary>
        private const double HandSize = 30;

        /// <summary>
        /// Thickness of drawn joint lines
        /// </summary>
        private const double JointThickness = 3;

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as closed
        /// </summary>
        private readonly Brush handClosedBrush = new SolidColorBrush(Color.FromArgb(128, 255, 0, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as opened
        /// </summary>
        private readonly Brush handOpenBrush = new SolidColorBrush(Color.FromArgb(128, 0, 255, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as in lasso (pointer) position
        /// </summary>
        private readonly Brush handLassoBrush = new SolidColorBrush(Color.FromArgb(128, 0, 0, 255));

        /// <summary>
        /// Size of the RGB pixel in the bitmap
        /// </summary>
        private readonly int bytesPerPixel = (PixelFormats.Bgr32.BitsPerPixel + 7) / 8;

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor kinectSensor = null;

        /// <summary>
        /// Reader for body
        /// frames
        /// </summary>
        private ColorFrameReader colorFrameReader = null;

        /// <summary>
        /// Reader for body frames
        /// </summary>
        private BodyFrameReader bodyFrameReader = null;

        /// <summary>
        /// Width of display (depth space)
        /// </summary>
        private int displayWidth;

        /// <summary>
        /// Height of display (depth space)
        /// </summary>
        private int displayHeight;

        /// <summary>
        /// Array for the bodies
        /// </summary>
        private Body[] bodies = null;

        /// <summary>
        /// Drawing group for body rendering output
        /// </summary>
        private DrawingGroup drawingGroup;

        /// <summary>
        /// Drawing image that we will display
        /// </summary>
        private DrawingImage imageSource;

        /// <summary>
        /// Bitmat Image that we will return for PoseGuide WPF control
        /// </summary>
        private BitmapSource poseGuideImage;

        /// <summary>
        /// Coordinate mapper to map one type of point to another
        /// </summary>
        private CoordinateMapper coordinateMapper = null;

        /// <summary>
        /// Pen used for drawing bones that are currently inferred
        /// </summary>        
        private readonly Pen inferredBonePen = new Pen(Brushes.Gray, 1);

        /// <summary>
        /// Pen used for drawing bones that are currently tracked
        /// </summary>
        private readonly Pen trackedBonePen = new Pen(Brushes.Green, 6);

        /// <summary>
        /// Brush used for drawing joints that are currently tracked
        /// </summary>
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));

        /// <summary>
        /// Brush used for drawing joints that are currently inferred
        /// </summary>        
        private readonly Brush inferredJointBrush = Brushes.Yellow;

        /// <summary>
        /// Timer to record data at specific interval
        /// </summary>
        private readonly DispatcherTimer timer = new DispatcherTimer()
        {
            // Tick fot test
            Interval = new TimeSpan(0, 0, 0, 0, 100),

        };


        /// <summary>
        /// Fake timer to test with redLED bitmap instead of color frames
        /// </summary>
        private readonly DispatcherTimer fakeTimer = new DispatcherTimer();

        /// <summary>
        /// Flash animation when recording data
        /// </summary>
        private readonly DoubleAnimation flashAnim = new DoubleAnimation
        {
            From = 1.0,
            To = 0.0,
            Duration = new Duration(TimeSpan.FromMilliseconds(100)),
            AutoReverse = true
        };

        /// <summary>
        /// Quick flash for LED in preparing state
        /// </summary>
        private readonly DoubleAnimation quickFlash = new DoubleAnimation
        {
            From = 1.0,
            To = 0.0,
            Duration = new Duration(TimeSpan.FromMilliseconds(50)),
            AutoReverse = true
        };

        /// <summary>
        /// Initiate bitmaps for red and green leds
        /// </summary>
        private readonly BitmapImage greenLED;
        private readonly BitmapImage redLED;

        private Image[] leds = null;

        /// <summary>
        /// Storyboard for KinectCanvas and LEDs
        /// </summary>
        private readonly Storyboard storyBoard = new Storyboard();
        private readonly Storyboard ledStoryBoard = new Storyboard();

        /// <summary>
        /// Default path: current path
        /// </summary>
        private readonly string defaultPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        /// <summary>
        /// Background worker to create data package
        /// </summary>
        private readonly BackgroundWorker packageBackgroundWorkder = new BackgroundWorker { WorkerSupportsCancellation = true };

        private readonly BackgroundWorker recordBackgroundWorker = new BackgroundWorker { WorkerSupportsCancellation = true };

        private WriteableBitmap realTimeImage = null;

        /// <summary>
        /// Time window in milliseconds
        /// </summary>
        private readonly TimeSpan captureTimeWindow = TimeSpan.FromMilliseconds(500);

        /// <summary>
        /// Pose matcher
        /// </summary>
        private readonly KinectLib.PoseMatcher poseMatcher = new KinectLib.PoseMatcher();

        private Timer poseSeriesTimer;

        #endregion

        /// <summary>
        /// Training of the form fist
        /// </summary>
        private FormTraining form;
        /// <summary>
        /// Voice interactive
        /// </summary>
        private VoiceControl voice;
        /// <summary>
        /// Stream for 32b-16b conversion.
        /// </summary>
        private KinectAudioStream kinectAudioStream;

        /// <summary>
        /// Mainly for voice command confirmation
        /// </summary>
        private ConfirmDialog confirmDialog;

        public WriteableBitmap RealTimeImage
        {
            get { return realTimeImage; }
            set
            {
                realTimeImage = value;
                NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// This only accessed by a method LogPose
        /// </summary>
        private readonly StringBuilder _poseLog = new StringBuilder();

        private string _nextPose;

        public string PoseLogText
        {
            get { return _poseLog.ToString(); }
            set
            {
                _poseLog.Clear();
                _poseLog.Append(value);
                NotifyPropertyChanged("PoseLogText");
            }
        }

        /// <summary>
        /// To be binded with NextPose textblock
        /// </summary>
        public string NextPoseText
        {
            get { return _nextPose; }
            set
            {
                _nextPose = value;
                NotifyPropertyChanged("NextPoseText");

            }
        }

        public void LogPose(PoseType pose)
        {
            _poseLog.AppendLine(pose.ToString());
            NotifyPropertyChanged("PoseLogText");
        }

        public MainWindow()
        {
            this.kinectSensor = KinectSensor.GetDefault();
            if (this.kinectSensor != null)
            {
                this.coordinateMapper = this.kinectSensor.CoordinateMapper;
                this.kinectSensor.Open();

                FrameDescription colorFrameDescription = this.kinectSensor.ColorFrameSource.FrameDescription;
                FrameDescription bodyFrameDescription = this.kinectSensor.DepthFrameSource.FrameDescription;
                this.displayWidth = colorFrameDescription.Width;
                this.displayHeight = colorFrameDescription.Height;
                this.bodies = new Body[this.kinectSensor.BodyFrameSource.BodyCount];
                this.colorFrameReader = this.kinectSensor.ColorFrameSource.OpenReader();
                this.bodyFrameReader = this.kinectSensor.BodyFrameSource.OpenReader();
                this.drawingGroup = new DrawingGroup();
                this.imageSource = new DrawingImage(this.drawingGroup);
                this.DataContext = this;

                // Setup voice control
                // Grab the audio stream
                IReadOnlyList<AudioBeam> audioBeamList = this.kinectSensor.AudioSource.AudioBeams;
                System.IO.Stream audioStream = audioBeamList[0].OpenInputStream();
                // Create the convert stream
                this.kinectAudioStream = new KinectAudioStream(audioStream);
                this.kinectAudioStream.SpeechActive = true;
                this.voice = new VoiceControl();
                this.voice.SetInputAutoStream(this.kinectAudioStream);
                this.voice.OnMatch += voice_OnMatch;
            }

            InitializeComponent();

            Assembly assembly = typeof(KinectLib.Extensions).Assembly;
            this.greenLED = new BitmapImage();
            this.greenLED.BeginInit();
            this.greenLED.StreamSource = assembly.GetManifestResourceStream("KinectLib.Resources.greenled.png");
            this.greenLED.EndInit();
            this.redLED = new BitmapImage();
            this.redLED.BeginInit();
            this.redLED.StreamSource = assembly.GetManifestResourceStream("KinectLib.Resources.redled.png");
            this.redLED.EndInit();

            this.Beep.Source = new Uri("Resources/beep.wav", UriKind.Relative);


            // Set storyboard for animation
            storyBoard.Children.Add(flashAnim);
            Storyboard.SetTargetName(flashAnim, "KinectCanvas");
            Storyboard.SetTargetProperty(flashAnim, new PropertyPath(Image.OpacityProperty));

            ledStoryBoard.Children.Add(quickFlash);
            Storyboard.SetTarget(quickFlash, this.LEDBoard);
            Storyboard.SetTargetProperty(quickFlash, new PropertyPath(StackPanel.OpacityProperty));

            // Setup the pose timer
            this.poseNotify = false;
            this.poseSeriesTimer = new Timer(500);
            this.poseSeriesTimer.Elapsed += new ElapsedEventHandler(ValidateKinect);

            // Setup the form training
            this.form = new FormTraining(PowerForm.FormOne);
            this.form.OnStart += form_OnStart;
            this.form.OnPause += form_OnPause;
            this.form.OnStop += form_OnStop;
            this.form.OnFinish += form_OnFinish;
            this.form.OnMatch += form_OnMatch;
            this.form.OnProceed += form_OnProceed;

        }

        void form_OnStop(object sender, EventArgs e)
        {
            this.NextPoseText = string.Empty;
            this.PoseGuideImage = null;
        }


        void voice_OnMatch(object sender, VoiceEventArgs e)
        {
            foreach (Span span in this.CommandSpans.Inlines.OfType<Span>())
            {
                if (e.Command.ToString().Equals(span.Tag))
                {
                    span.Foreground = Brushes.DeepSkyBlue;
                    span.FontWeight = FontWeights.Bold;
                }
                else
                {
                    span.Foreground = (Brush)this.Resources["MediumGreyBrush"];
                    span.FontWeight = FontWeights.Normal;
                }
            }
            switch (e.Command)
            {
                case VoiceCommand.Start:
                    this.StartForm();
                    break;
                case VoiceCommand.Stop:
                    this.StopForm();
                    break;
                case VoiceCommand.Quit:
                    this.Quit();
                    break;
                case VoiceCommand.Yes:
                    this.ConfirmYes();
                    break;
                case VoiceCommand.No:
                    this.ConfirmNo();
                    break;
            }
        }

        private void ConfirmNo()
        {
            if (!this.confirmDialog.IsShowingDialog) return;
            switch (this.confirmDialog.CurrentState)
            {
                case FormTrainingState.Running:
                    this.NextPoseText = this.form.PoseSequence[this.form.NextPose].ToString();
                    this.form.State = FormTrainingState.Running;
                    break;
                case FormTrainingState.Finished:
                    this.NextPoseText = "FINISH!";
                    this.form.State = FormTrainingState.Finished;
                    break;
            }
            this.IsEnabled = true;
            this.confirmDialog.Hide();
            this.confirmDialog.IsShowingDialog = false;
        }

        private void ConfirmYes()
        {
            if (!this.confirmDialog.IsShowingDialog) return;
            switch (this.confirmDialog.ActiveCommand)
            {
                case VoiceCommand.Start:
                    this.form.Start();
                    break;
                case VoiceCommand.Stop:
                    this.form.Stop();
                    break;
                case VoiceCommand.Quit:
                    this.Close();
                    break;
            }
            this.IsEnabled = true;
            this.confirmDialog.Hide();
            this.confirmDialog.IsShowingDialog = false;
        }

        private void Quit()
        {
            if (!this.confirmDialog.IsShowingDialog)
            {
                this.form.Pause();
                this.confirmDialog.Show(form.State, VoiceCommand.Quit);
            }
        }

        private void StopForm()
        {
            if (this.form.State == FormTrainingState.Paused) return;
            if (!this.confirmDialog.IsShowingDialog)
            {
                this.confirmDialog.Show(form.State, VoiceCommand.Stop);
                this.form.Pause();
            }
        }

        private void StartForm()
        {
            if (this.form.State == FormTrainingState.Running) return;
            if (!this.confirmDialog.IsShowingDialog)
            {
                this.form.Pause();
                this.confirmDialog.Show(form.State, VoiceCommand.Start);
            }
        }

        #region Form Training Event Listeners

        void form_OnProceed(object sender, PoseEventArgs e)
        {
            var pose = this.form.PoseSequence[this.form.NextPose + 1];

            var bitmap = (System.Drawing.Bitmap)Properties.Resources.ResourceManager.GetObject(
                pose.ToString());
            if (null == bitmap)
            {
                bitmap = Properties.Resources.PoseNotAvailable;
            }
            this.PoseGuideImage = Imaging.CreateBitmapSourceFromHBitmap(
               bitmap.GetHbitmap(),
               IntPtr.Zero,
               System.Windows.Int32Rect.Empty,
               BitmapSizeOptions.FromEmptyOptions());

            this.NextPoseText = pose.GetName();

            this.Beep.Play();
        }

        void form_OnMatch(object sender, PoseEventArgs e)
        {
            //Do nothing
        }
        void form_OnFinish(object sender, EventArgs e)
        {
            this.NextPoseText = "FINISH!";
        }

        void form_OnPause(object sender, EventArgs e)
        {
            this.NextPoseText = "PAUSED";
        }
        void form_OnStart(object sender, EventArgs e)
        {
            // Do nothing
        }

        #endregion

        /// <summary>
        /// Finished notifying user
        /// </summary>
        private void ValidateKinect(object sender, ElapsedEventArgs e)
        {
            this.poseNotify = false;
        }

        /// <summary>
        /// Prepare the effects and events
        /// </summary>
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.colorFrameReader != null)
            {
                this.colorFrameReader.FrameArrived += this.ColorFrameReaderFrameArrived;
            }

            if (this.bodyFrameReader != null)
            {
                this.bodyFrameReader.FrameArrived += this.BodyFrameReaderFrameArrived;
            }

            // Setup the LEDs
            this.leds = (from c in this.LEDBoard.Children.OfType<Image>()
                         where "LED".Equals(c.Tag)
                         select c).ToArray<Image>();
            foreach (Image l in leds)
            {
                l.Source = redLED;
            }

            // Setup the confirm dialog
            this.confirmDialog = new ConfirmDialog("Click or say \"YES\" or \"NO\"");
            this.confirmDialog.Owner = this;
            this.confirmDialog.Closing += confirmDialog_Closing;

        }

        void confirmDialog_Closing(object sender, CancelEventArgs e)
        {
           // Hide, and cancel closing
            switch (this.confirmDialog.Answer)
            {
                case VoiceCommand.Yes:
                    this.ConfirmYes();
                    break;
                case VoiceCommand.No:
                    this.ConfirmNo();
                    break;
            }
            e.Cancel = true;
        }





        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        private bool poseNotify;


        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
            if (propertyName == "PoseLogText")
            {
                this.PoseScroll.ScrollToBottom();

            }
        }

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource ImageSource
        {
            get
            {
                return this.imageSource;
            }
        }

        public BitmapSource PoseGuideImage
        {
            get
            {
                return this.poseGuideImage;
            }
            set
            {
                this.poseGuideImage = value;
                this.NotifyPropertyChanged("PoseGuideImage");
            }
        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (this.colorFrameReader != null)
            {
                // ColorFrameReader is IDisposable
                this.colorFrameReader.Dispose();
                this.colorFrameReader = null;

                // BodyFrameReader is IDisposable
                this.bodyFrameReader.Dispose();
                this.bodyFrameReader = null;
            }

            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }

            if (null != this.kinectAudioStream)
            {
                this.kinectAudioStream.SpeechActive = false;
            }

            if (this.voice != null)
            {
                this.voice.Dispose();
            }


        }

        private void ColorFrameReaderFrameArrived(object sender, ColorFrameArrivedEventArgs e)
        {
            ColorFrameReference frameReference = e.FrameReference;
            ColorFrame frame = frameReference.AcquireFrame();

            if (frame != null)
            {
                // ColorFrame is IDisposable
                using (frame)
                {
                    FrameDescription frameDescription = frame.CreateFrameDescription(ColorImageFormat.Bgra);
                    int width = frameDescription.Width;
                    int height = frameDescription.Height;
                    uint bufferLength = (uint)(frameDescription.LengthInPixels * frameDescription.BytesPerPixel);

                    if (null == realTimeImage)
                    {
                        realTimeImage = new WriteableBitmap(width, height, 96, 96, PixelFormats.Bgra32, null);
                    }
                    realTimeImage.Lock();
                    frame.CopyConvertedFrameDataToIntPtr(realTimeImage.BackBuffer, bufferLength, ColorImageFormat.Bgra);
                    realTimeImage.AddDirtyRect(new Int32Rect(0, 0, width, height));
                    realTimeImage.Unlock();
                    //NotifyPropertyChanged("RealTimeImage");

                }
            }
        }


        private void BodyFrameReaderFrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            BodyFrameReference frameReference = e.FrameReference;
            // If nothing is tracked, null the joints reference
            BodyFrame frame = frameReference.AcquireFrame();

            if (frame != null)
            {
                // BodyFrame is IDisposable
                using (frame)
                {
                    using (DrawingContext dc = this.drawingGroup.Open())
                    {
                        // Draw a transparent background to set the render size

                        dc.DrawRectangle(Brushes.Transparent, null, new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));

                        // draw the kinect bitmap if it's there
                        if (null != RealTimeImage)
                        {
                            // determine the coordinates for displaying the image
                            Double w = realTimeImage.Width * this.displayHeight / realTimeImage.Height;
                            Double diffWidth = Math.Abs(this.displayWidth - w);
                            Double x = -(diffWidth / 2);
                            Double ww = w + x;
                            dc.DrawImage(realTimeImage, new Rect(x, 0.0, w, this.displayHeight));
                        }

                        // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                        // As long as those body objects are not disposed and not set to null in the array,
                        // those body objects will be re-used.
                        frame.GetAndRefreshBodyData(this.bodies);

                        foreach (Body body in this.bodies)
                        {
                            if (body.IsTracked)
                            {
                                // Create double array from joint set, we need to create a deep copy first
                                var jointsCopy = body.Joints.ToDictionary(
                                    entry => entry.Key, entry => entry.Value);
                                var jointOrienationsCopy = body.JointOrientations.ToDictionary(
                                    entry => entry.Key, entry => entry.Value);
                                var simpleBody = new SimpleBody()
                                {
                                    Joints = jointsCopy,
                                    JointOrientations = jointOrienationsCopy,
                                    FloorClipPlane = frame.FloorClipPlane
                                };
                                this.form.AddBody(simpleBody);
                                var pose = this.poseMatcher.Match(simpleBody);
                                if (pose != PoseType.Unknown)
                                {
                                    if (!this.poseNotify)
                                    {
                                        this.poseNotify = true;
                                        this.poseSeriesTimer.Start();
                                        LogPose(pose);
                                        this.ledStoryBoard.Begin();
                                    }
                                }

                                // Record pose if still in capture time window
                                DateTime currentTime = DateTime.Now;
                                // convert the joint points to depth (display) space
                                Dictionary<JointType, Point> jointPoints = new Dictionary<JointType, Point>();
                                foreach (JointType jointType in body.Joints.Keys)
                                {
                                    ColorSpacePoint colorSpacePoint = this.coordinateMapper.MapCameraPointToColorSpace(body.Joints[jointType].Position);
                                    jointPoints[jointType] = new Point(colorSpacePoint.X, colorSpacePoint.Y);
                                }
                                this.DrawBody(body.Joints, jointPoints, dc);
                                this.DrawHand(body.HandLeftState, jointPoints[JointType.HandLeft], dc);
                                this.DrawHand(body.HandRightState, jointPoints[JointType.HandRight], dc);

                            }
                        }
                        // prevent drawing outside of our render area
                        this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));

                    }
                }

            }
        }

        #region Canvas Drawing Methods
        /// <summary>
        /// Draws a hand symbol if the hand is tracked: red circle = closed, green circle = opened; blue circle = lasso
        /// </summary>
        /// <param name="handState">state of the hand</param>
        /// <param name="handPosition">position of the hand</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawHand(HandState handState, Point handPosition, DrawingContext drawingContext)
        {
            switch (handState)
            {
                case HandState.Closed:
                    drawingContext.DrawEllipse(this.handClosedBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Open:
                    drawingContext.DrawEllipse(this.handOpenBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Lasso:
                    drawingContext.DrawEllipse(this.handLassoBrush, null, handPosition, HandSize, HandSize);


                    break;
            }
        }

        /// <summary>
        /// Draws one bone of a body (joint to joint)
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="jointType0">first joint of bone to draw</param>
        /// <param name="jointType1">second joint of bone to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawBone(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, Point> jointPoints, JointType jointType0, JointType jointType1, DrawingContext drawingContext)
        {
            Joint joint0 = joints[jointType0];
            Joint joint1 = joints[jointType1];

            // If we can't find either of these joints, exit
            if (joint0.TrackingState == TrackingState.NotTracked ||
                joint1.TrackingState == TrackingState.NotTracked)
            {
                return;
            }

            // Don't draw if both points are inferred
            if (joint0.TrackingState == TrackingState.Inferred &&
                joint1.TrackingState == TrackingState.Inferred)
            {
                return;
            }

            // We assume all drawn bones are inferred unless BOTH joints are tracked
            Pen drawPen = this.inferredBonePen;
            if ((joint0.TrackingState == TrackingState.Tracked) && (joint1.TrackingState == TrackingState.Tracked))
            {
                drawPen = this.trackedBonePen;
            }

            drawingContext.DrawLine(drawPen, jointPoints[jointType0], jointPoints[jointType1]);
        }

        /// <summary>
        /// Draws a body
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawBody(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, Point> jointPoints, DrawingContext drawingContext)
        {
            // Draw the bones

            // Torso
            this.DrawBone(joints, jointPoints, JointType.Head, JointType.Neck, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.Neck, JointType.SpineShoulder, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.SpineMid, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineMid, JointType.SpineBase, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.ShoulderRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.ShoulderLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineBase, JointType.HipRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineBase, JointType.HipLeft, drawingContext);

            // Right Arm    
            this.DrawBone(joints, jointPoints, JointType.ShoulderRight, JointType.ElbowRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.ElbowRight, JointType.WristRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristRight, JointType.HandRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.HandRight, JointType.HandTipRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristRight, JointType.ThumbRight, drawingContext);

            // Left Arm
            this.DrawBone(joints, jointPoints, JointType.ShoulderLeft, JointType.ElbowLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.ElbowLeft, JointType.WristLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristLeft, JointType.HandLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.HandLeft, JointType.HandTipLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristLeft, JointType.ThumbLeft, drawingContext);

            // Right Leg
            this.DrawBone(joints, jointPoints, JointType.HipRight, JointType.KneeRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.KneeRight, JointType.AnkleRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.AnkleRight, JointType.FootRight, drawingContext);

            // Left Leg
            this.DrawBone(joints, jointPoints, JointType.HipLeft, JointType.KneeLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.KneeLeft, JointType.AnkleLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.AnkleLeft, JointType.FootLeft, drawingContext);

            // Draw the joints
            foreach (JointType jointType in joints.Keys)
            {
                Brush drawBrush = null;

                TrackingState trackingState = joints[jointType].TrackingState;

                if (trackingState == TrackingState.Tracked)
                {
                    drawBrush = this.trackedJointBrush;
                }
                else if (trackingState == TrackingState.Inferred)
                {
                    drawBrush = this.inferredJointBrush;
                }

                if (drawBrush != null)
                {
                    drawingContext.DrawEllipse(drawBrush, null, jointPoints[jointType], JointThickness, JointThickness);
                }
            }
        }
        #endregion

        /// <summary>
        /// When sound ends, stop to seek the beginning
        /// </summary>
        private void Beep_MediaEnded(object sender, RoutedEventArgs e)
        {
            Beep.Stop();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var i = this.LanguageBox.SelectedItem as ComboBoxItem;
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo((string)i.Tag);
            if (this.form != null)
            {
                this.NextPoseText = this.form.PoseSequence[this.form.NextPose].GetName();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.StartForm();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.StopForm();
        }

    }


}
