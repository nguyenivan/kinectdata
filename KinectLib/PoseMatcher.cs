﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SVM;
using System.Reflection;


namespace KinectLib
{
    public class PoseMatcher
    {
        public static int FEATURES_COUNT = SimpleBody.selectedJoints.Length * 4;
        public PoseMatcher() { }
        private readonly Node[] features = new Node[FEATURES_COUNT];
        private readonly Model model = Model.Read(Assembly.GetExecutingAssembly()
                .GetManifestResourceStream("KinectLib.Resources.model.txt"));

        /// <summary>
        /// Match a body against the svm model(currently 16 joints = 64 features)
        /// </summary>
        /// <param name="body">The body</param>
        /// <returns>Pose matched</returns>
        public PoseType Match(SimpleBody body)
        {
            Array.Clear(features, 0, FEATURES_COUNT);
            var localRotations = body.GetLocalOrientations();
            var i = 0;
            ///TODO: What if some joint is missing in the local rotations dict?
            foreach (var joint in SimpleBody.selectedJoints)
            {
                if (localRotations.ContainsKey(joint))
                {
                    features[i*4 + 0] = new Node(i*4 + 1, localRotations[joint].W);
                    features[i*4 + 1] = new Node(i*4 + 2, localRotations[joint].X);
                    features[i*4 + 2] = new Node(i*4 + 3, localRotations[joint].Y);
                    features[i*4 + 3] = new Node(i*4 + 4, localRotations[joint].Z);
                }
                i++;
            }
            double predict = Prediction.Predict(this.model, features);
            return (PoseType)Convert.ToInt32(predict);
        }
    }

    public enum PoseType
    {
        Unknown = 0,
        DungThang,
        LapTan,
        VanVo,
        TuBinh,
        HoTraoTrai,
        ThoiSonPhai,
        HoiChoPhai,
        TramKieuPhai,
        HoTraoPhai,
        ThoiSonTrai,
        HoiChoTrai,
        TramKieuTrai,
        QuaiChuyTrai
    }
}
