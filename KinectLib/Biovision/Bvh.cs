﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using Vector4 = OpenTK.Vector4;

namespace KinectLib.Biovision
{
    public class Bvh
    {
        /// <summary>
        /// Number of frames to be  skipped before MoCap
        /// </summary>
        public int NumberOfSkippedFrames = 1;

        /// <summary>
        /// Frames to be averaged to come up with Hierarchy Header
        /// </summary>
        public int NumberOfInitFrames = 4;

        private List<BvhFrame> frames = new List<BvhFrame>();

        /// <summary>
        /// Temp list to keep hierarchy information before average out
        /// </summary>
        //private List<BvhHierarchy> tempHierarchy = new List<BvhHierarchy>();

        //private bool tempHierarchyIsFull = false;

        /// <summary>
        /// Adjust this field to avoid unstable body frames
        /// </summary>
        private int startAt = 10;

        /// <summary>
        /// Flag to indicate if we are working on header part or motion part
        /// </summary>
        private bool isInitialized = false;

        /// <summary>
        /// Temp list to keep bodies information before average out
        /// </summary>
        //private List<SimpleBody> tempBodies = new List<SimpleBody>();

        public static float UNIT_SCALE = 100;
        private int bodyCount = 0;

        public Bvh() { }

        public BvhHierarchy BvhHierarchy { get; private set; }

        public void AddFrame(SimpleBody body)
        {
            //if (!tempHierarchyIsFull)
            if (!isInitialized)
            {
                AddHierarchy(body);
            }
            else
            {
                AddMotion2(body);
            }
        }

        /// <summary>
        /// Add one body to the temp hierarchy
        /// Assign tempHierarchyIsFull where needed
        /// </summary>
        /// <param name="body"></param>
        private void AddHierarchy(SimpleBody body)
        {
            this.bodyCount++;

            if (this.isInitialized) return;
            if (this.bodyCount < this.startAt) return; // Skip this body

            BvhHierarchy hierarchy = new BvhHierarchy(BvhType.Biped);
            Vector3 rootOffset = Extensions.AdjustKinectCoords(0, 0, 0);
            hierarchy.SetJointOffset(
                BvhHierarchy.RootJoint, rootOffset
            );
            // Set reference too
            var rootK = body.Joints[BvhHierarchy.JointsMapping[BvhHierarchy.RootJoint].Value].Position;
            hierarchy.Reference = Extensions.AdjustKinectCoords(rootK.X, rootK.Y, rootK.Z) * UNIT_SCALE;

            // Traverse the joint tree and assign offset
            foreach (BvhJointType child in BvhHierarchy.Traverse())
            {
                if (child == BvhHierarchy.RootJoint) continue;
                if (!BvhHierarchy.JointsMapping[child].HasValue)
                {
                    hierarchy.SetJointOffset(child, Extensions.AdjustKinectCoords(0f, 0f, 0f));
                    continue;
                }
                BvhJointType parent = BvhHierarchy.FindParent(child);
                var kinectChildJoint = body.Joints[BvhHierarchy.JointsMapping[child].Value];
                var kinectParentJoint = body.Joints[BvhHierarchy.JointsMapping[parent].Value];

                Vector3 parentPosition = Extensions.AdjustKinectCoords(
                    kinectParentJoint.Position.X * UNIT_SCALE,
                    kinectParentJoint.Position.Y * UNIT_SCALE,
                    kinectParentJoint.Position.Z * UNIT_SCALE);
                Vector3 childPosition = Extensions.AdjustKinectCoords(
                    kinectChildJoint.Position.X * UNIT_SCALE,
                    kinectChildJoint.Position.Y * UNIT_SCALE,
                    kinectChildJoint.Position.Z * UNIT_SCALE);

                hierarchy.SetJointOffset(child, childPosition - parentPosition);

                // If this is "End Site", try to find the corresponding Kinect child joint and set offset
                if (!BvhHierarchy.BvhJointRelationships.ContainsKey(child) || BvhHierarchy.BvhJointRelationships[child].Count == 0)
                {
                    var endSiteParent = BvhHierarchy.JointsMapping[child];
                    if (BvhHierarchy.JointsMapping[child].HasValue &&
                        BvhHierarchy.JointRelationships.ContainsKey(endSiteParent.Value) &&
                        BvhHierarchy.JointRelationships[endSiteParent.Value].Count > 0)
                    {
                        var kinectEndSiteJoint = body.Joints[BvhHierarchy.JointRelationships[endSiteParent.Value][0]];

                        Vector3 endSitePosition = Extensions.AdjustKinectCoords(
                           kinectEndSiteJoint.Position.X * UNIT_SCALE,
                           kinectEndSiteJoint.Position.Y * UNIT_SCALE,
                           kinectEndSiteJoint.Position.Z * UNIT_SCALE);
                        hierarchy.SetEndSiteOffset(child, endSitePosition - childPosition);
                    }
                }
            }
            this.BvhHierarchy = hierarchy;
            this.Body = body;
            this.isInitialized = true;

            //// Do not have to convert some frames at beginning
            //if (this.tempHierarchy.Count > this.NumberOfSkippedFrames)
            //{
            //    this.tempBodies.Add(body);
            //    Vector3 rootOffset = Extensions.AdjustKinectCoords(0, 0, 0);
            //    hierarchy.SetJointOffset(
            //        BvhHierarchy.RootJoint, rootOffset
            //    );
            //    // Set reference too
            //    var rootK = body.Joints[BvhHierarchy.JointsMapping[BvhHierarchy.RootJoint].Value].Position;
            //    hierarchy.Reference = Extensions.AdjustKinectCoords(rootK.X, rootK.Y, rootK.Z) * UNIT_SCALE;

            //    // Traverse the joint tree and assign offset
            //    foreach (BvhJointType child in BvhHierarchy.Traverse())
            //    {
            //        if (child == BvhHierarchy.RootJoint) continue;
            //        if (!BvhHierarchy.JointsMapping[child].HasValue)
            //        {
            //            hierarchy.SetJointOffset(child, Extensions.AdjustKinectCoords(0f, 0f, 0f));
            //            continue;
            //        }
            //        BvhJointType parent = BvhHierarchy.FindParent(child);
            //        var kinectChildJoint = body.Joints[BvhHierarchy.JointsMapping[child].Value];
            //        var kinectParentJoint = body.Joints[BvhHierarchy.JointsMapping[parent].Value];

            //        Vector3 parentPosition = Extensions.AdjustKinectCoords(
            //            kinectParentJoint.Position.X * UNIT_SCALE,
            //            kinectParentJoint.Position.Y * UNIT_SCALE,
            //            kinectParentJoint.Position.Z * UNIT_SCALE);
            //        Vector3 childPosition = Extensions.AdjustKinectCoords(
            //            kinectChildJoint.Position.X * UNIT_SCALE,
            //            kinectChildJoint.Position.Y * UNIT_SCALE,
            //            kinectChildJoint.Position.Z * UNIT_SCALE);

            //        hierarchy.SetJointOffset(child, childPosition - parentPosition);

            //        // If this is "End Site", try to find the corresponding Kinect child joint and set offset
            //        if (!BvhHierarchy.BvhJointRelationships.ContainsKey(child) || BvhHierarchy.BvhJointRelationships[child].Count == 0)
            //        {
            //            var endSiteParent = BvhHierarchy.JointsMapping[child];
            //            if (BvhHierarchy.JointsMapping[child].HasValue &&
            //                BvhHierarchy.JointRelationships.ContainsKey(endSiteParent.Value) &&
            //                BvhHierarchy.JointRelationships[endSiteParent.Value].Count > 0)
            //            {
            //                var kinectEndSiteJoint = body.Joints[BvhHierarchy.JointRelationships[endSiteParent.Value][0]];

            //                Vector3 endSitePosition = Extensions.AdjustKinectCoords(
            //                   kinectEndSiteJoint.Position.X * UNIT_SCALE,
            //                   kinectEndSiteJoint.Position.Y * UNIT_SCALE,
            //                   kinectEndSiteJoint.Position.Z * UNIT_SCALE);
            //                hierarchy.SetEndSiteOffset(child, endSitePosition - childPosition);
            //            }
            //        }
            //    }

            //}

            //if (this.tempHierarchy.Count >= NumberOfSkippedFrames + NumberOfInitFrames)
            //{
            //    this.tempHierarchyIsFull = true;
            //    this.BvhHierarchy = this.AverageHierarchy();
            //    //Calculate average body information
            //    this.Body = SimpleBody.Average(this.tempBodies);
            //}
        }

        private void AddMotion2(SimpleBody body)
        {
            this.bodyCount++;
            if (!this.isInitialized) return;
            if (null == this.Body)
            {
                throw new Exception("Original body cannot be null");
            }
            if (null == this.BvhHierarchy)
            {
                throw new Exception("BvhHierachy property haven't been set (automatically or manually)");
            }
            var frame = new BvhFrame();
            foreach (BvhJointType bvhJointType in BvhHierarchy.JointsMapping.Keys)
            {
                if (
                    this.BvhHierarchy.Joints[bvhJointType].MotionChannels.Contains(MotionChannelType.Xrotation) ||
                    this.BvhHierarchy.Joints[bvhJointType].MotionChannels.Contains(MotionChannelType.Yrotation) ||
                    this.BvhHierarchy.Joints[bvhJointType].MotionChannels.Contains(MotionChannelType.Zrotation)
                    )
                {
                    //var jointType = bvhJointType == BvhHierarchy.RootJoint ?
                    //    BvhHierarchy.JointsMapping[bvhJointType] :
                    //    BvhHierarchy.JointsMapping[BvhHierarchy.FindParent(bvhJointType, false)];

                    // If this joint has more than 1 children, take the first one as orientation source
                    JointType? jointType = BvhHierarchy.BvhJointRelationships.ContainsKey(bvhJointType) ?
                        BvhHierarchy.JointsMapping[BvhHierarchy.BvhJointRelationships[bvhJointType][0]] :
                        null;

                    if (jointType.HasValue)
                    {
                        var orientationSource = BvhHierarchy.OrientationsSource[jointType.Value];
                        if (orientationSource.HasValue)
                        {
                            var qDiff = Extensions.DiffQuaternion(this.Body, body, orientationSource.Value);
                            var rotation = EulerRotation.FromQ(qDiff);
                            frame.AddMotion(bvhJointType, rotation);
                        }
                        else
                        {
                            frame.AddMotion(bvhJointType, new EulerRotation(0, 0, 0));
                        }
                    }
                    else
                    {
                        var rotation = new EulerRotation(0, 0, 0);
                        frame.AddMotion(bvhJointType, rotation);
                    }
                }

                // Translation motion
                if (this.BvhHierarchy.Joints[bvhJointType].MotionChannels.Contains(MotionChannelType.Xposition) ||
                    this.BvhHierarchy.Joints[bvhJointType].MotionChannels.Contains(MotionChannelType.Yposition) ||
                    this.BvhHierarchy.Joints[bvhJointType].MotionChannels.Contains(MotionChannelType.Zposition)
                    )
                {
                    if (bvhJointType != BvhHierarchy.RootJoint)
                    {
                        throw new NotImplementedException("Translation motion channels are not implemented for joint other than ROOT yet.");
                    }
                    var rootK = body.Joints[BvhHierarchy.JointsMapping[BvhHierarchy.RootJoint].Value].Position;
                    var rootPrime = Extensions.AdjustKinectCoords(
                        rootK.X, rootK.Y, rootK.Z
                    ) * UNIT_SCALE;
                    frame.AddMotion(bvhJointType, rootPrime - this.BvhHierarchy.Reference);
                }
            }

            this.frames.Add(frame);

        }

        public BvhHierarchy AverageHierarchy()
        {
            throw new NotImplementedException();
            //return tempHierarchy[this.NumberOfSkippedFrames];
            //var offsetDict = BvhHierarchy.JointsMapping.ToDictionary(
            //    x => x.Key, x => new List<Vector3>()
            //);
            //var endSiteDict = BvhHierarchy.JointsMapping.ToDictionary(
            //    x => x.Key, x => new List<Vector3>()
            //);

            //var reference = new Vector3();

            //for (int i = this.NumberOfSkippedFrames; i < this.tempHierarchy.Count; i++)
            //{
            //    foreach (BvhJointType key in offsetDict.Keys)
            //    {
            //        var allOffsets = this.tempHierarchy[i].GetAllJointOffsets();
            //        allOffsets.ToList().ForEach(x => offsetDict[x.Key].Add(x.Value));

            //        var allEndsites = this.tempHierarchy[i].GetAllEndSiteOffsets();
            //        allEndsites.ToList().ForEach(x => endSiteDict[x.Key].Add(x.Value));
            //    }
            //    reference.Add(this.tempHierarchy[i].Reference);
            //}

            //// Average out
            //BvhHierarchy hierarchy = new BvhHierarchy();
            //offsetDict.ToList().ForEach(x =>
            //    {
            //        Vector3 tempOffset = new Vector3();
            //        x.Value.ForEach(y => tempOffset = tempOffset + y);
            //        tempOffset = tempOffset / x.Value.Count();
            //        hierarchy.SetJointOffset(x.Key, tempOffset);
            //    });
            //endSiteDict.ToList().ForEach(x =>
            //{
            //    Vector3 tempOffset = new Vector3();
            //    x.Value.ForEach(y => tempOffset = tempOffset + y);
            //    tempOffset = tempOffset / x.Value.Count();
            //    hierarchy.SetEndSiteOffset(x.Key, tempOffset);
            //});

            //hierarchy.Reference = reference / (this.tempHierarchy.Count - this.NumberOfSkippedFrames);
            //return hierarchy;
        }

        public string ExportHeader()
        {
            string template = string.Empty;
            using (StreamReader sr = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("KinectLib.Resources.bvhheader.txt")))
            {
                template = sr.ReadToEnd();
            }
            var hierarchy = this.BvhHierarchy;
            string retVal = template.Format(
                frame_count => 0,
                frame_time => 0.0333,
                offset_hips => hierarchy.GetJointOffset(BvhJointType.Hips).ToBareString(),
                offset_leftupleg => hierarchy.GetJointOffset(BvhJointType.LeftUpLeg).ToBareString(),
                offset_leftleg => hierarchy.GetJointOffset(BvhJointType.LeftLeg).ToBareString(),
                offset_leftfoot => hierarchy.GetJointOffset(BvhJointType.LeftFoot).ToBareString(),
                offset_lefttoebase => hierarchy.GetJointOffset(BvhJointType.LeftToeBase).ToBareString(),
                offset_rightupleg => hierarchy.GetJointOffset(BvhJointType.RightUpLeg).ToBareString(),
                offset_rightleg => hierarchy.GetJointOffset(BvhJointType.RightLeg).ToBareString(),
                offset_rightfoot => hierarchy.GetJointOffset(BvhJointType.RightFoot).ToBareString(),
                offset_righttoebase => hierarchy.GetJointOffset(BvhJointType.RightToeBase).ToBareString(),
                offset_spine => hierarchy.GetJointOffset(BvhJointType.Spine).ToBareString(),
                offset_leftshoulder => hierarchy.GetJointOffset(BvhJointType.LeftShoulder).ToBareString(),
                offset_leftarm => hierarchy.GetJointOffset(BvhJointType.LeftArm).ToBareString(),
                offset_leftforearm => hierarchy.GetJointOffset(BvhJointType.LeftForeArm).ToBareString(),
                offset_lefthand => hierarchy.GetJointOffset(BvhJointType.LeftHand).ToBareString(),
                offset_leftfingerbase => hierarchy.GetJointOffset(BvhJointType.LeftFingerBase).ToBareString(),
                offset_rightshoulder => hierarchy.GetJointOffset(BvhJointType.RightShoulder).ToBareString(),
                offset_rightarm => hierarchy.GetJointOffset(BvhJointType.RightArm).ToBareString(),
                offset_rightforearm => hierarchy.GetJointOffset(BvhJointType.RightForeArm).ToBareString(),
                offset_righthand => hierarchy.GetJointOffset(BvhJointType.RightHand).ToBareString(),
                offset_rightfingerbase => hierarchy.GetJointOffset(BvhJointType.RightFingerBase).ToBareString(),
                offset_neck => hierarchy.GetJointOffset(BvhJointType.Neck).ToBareString(),
                offset_head => hierarchy.GetJointOffset(BvhJointType.Head).ToBareString(),
                endsite_leftfingerbase => hierarchy.GetEndSiteOffset(BvhJointType.LeftFingerBase).ToBareString(),
                endsite_rightfingerbase => hierarchy.GetEndSiteOffset(BvhJointType.RightFingerBase).ToBareString(),
                endsite_lefttoebase => hierarchy.GetEndSiteOffset(BvhJointType.LeftToeBase).ToBareString(),
                endsite_righttoebase => hierarchy.GetEndSiteOffset(BvhJointType.RightToeBase).ToBareString(),
                endsite_head => hierarchy.GetEndSiteOffset(BvhJointType.Head).ToBareString()
            );
            return retVal;
        }

        public string ExportAll()
        {
            string template = string.Empty;
            using (StreamReader sr = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("KinectLib.Resources.bvhheader.txt")))
            {
                template = sr.ReadToEnd();
            }
            var hierarchy = this.BvhHierarchy;
            StringBuilder retVal = new StringBuilder(template.Format(
                frame_count => this.frames.Count + 1,
                frame_time => 0.0333,
                offset_hips => hierarchy.GetJointOffset(BvhJointType.Hips).ToBareString(),
                offset_leftupleg => hierarchy.GetJointOffset(BvhJointType.LeftUpLeg).ToBareString(),
                offset_leftleg => hierarchy.GetJointOffset(BvhJointType.LeftLeg).ToBareString(),
                offset_leftfoot => hierarchy.GetJointOffset(BvhJointType.LeftFoot).ToBareString(),
                offset_lefttoebase => hierarchy.GetJointOffset(BvhJointType.LeftToeBase).ToBareString(),
                offset_rightupleg => hierarchy.GetJointOffset(BvhJointType.RightUpLeg).ToBareString(),
                offset_rightleg => hierarchy.GetJointOffset(BvhJointType.RightLeg).ToBareString(),
                offset_rightfoot => hierarchy.GetJointOffset(BvhJointType.RightFoot).ToBareString(),
                offset_righttoebase => hierarchy.GetJointOffset(BvhJointType.RightToeBase).ToBareString(),
                offset_spine => hierarchy.GetJointOffset(BvhJointType.Spine).ToBareString(),
                offset_leftshoulder => hierarchy.GetJointOffset(BvhJointType.LeftShoulder).ToBareString(),
                offset_leftarm => hierarchy.GetJointOffset(BvhJointType.LeftArm).ToBareString(),
                offset_leftforearm => hierarchy.GetJointOffset(BvhJointType.LeftForeArm).ToBareString(),
                offset_lefthand => hierarchy.GetJointOffset(BvhJointType.LeftHand).ToBareString(),
                offset_leftfingerbase => hierarchy.GetJointOffset(BvhJointType.LeftFingerBase).ToBareString(),
                offset_rightshoulder => hierarchy.GetJointOffset(BvhJointType.RightShoulder).ToBareString(),
                offset_rightarm => hierarchy.GetJointOffset(BvhJointType.RightArm).ToBareString(),
                offset_rightforearm => hierarchy.GetJointOffset(BvhJointType.RightForeArm).ToBareString(),
                offset_righthand => hierarchy.GetJointOffset(BvhJointType.RightHand).ToBareString(),
                offset_rightfingerbase => hierarchy.GetJointOffset(BvhJointType.RightFingerBase).ToBareString(),
                offset_neck => hierarchy.GetJointOffset(BvhJointType.Neck).ToBareString(),
                offset_head => hierarchy.GetJointOffset(BvhJointType.Head).ToBareString(),
                endsite_leftfingerbase => hierarchy.GetEndSiteOffset(BvhJointType.LeftFingerBase).ToBareString(),
                endsite_rightfingerbase => hierarchy.GetEndSiteOffset(BvhJointType.RightFingerBase).ToBareString(),
                endsite_lefttoebase => hierarchy.GetEndSiteOffset(BvhJointType.LeftToeBase).ToBareString(),
                endsite_righttoebase => hierarchy.GetEndSiteOffset(BvhJointType.RightToeBase).ToBareString(),
                endsite_head => hierarchy.GetEndSiteOffset(BvhJointType.Head).ToBareString()
                )
            );
            retVal.Append("\n");
            for (int i = 0; i < 68; i++)
            {
                retVal.Append("0 ");
            }
            retVal.Append("0");

            // Motions
            foreach (var frame in this.frames)
            {
                bool beginLine = true;
                foreach (BvhJointType bvhJointType in BvhHierarchy.JointsMapping.Keys)
                {
                    foreach (MotionChannelType channelType in this.BvhHierarchy.Joints[bvhJointType].MotionChannels)
                    {
                        retVal.Append((beginLine ? "\n" : " ") + frame.MotionChannels[channelType][bvhJointType].ToString());
                        beginLine = false;
                    }
                }
            }

            return retVal.ToString();

        }

        /// <summary>
        /// Average body: joint orientations and positions
        /// </summary>
        public SimpleBody Body { get; set; }
    }
}

#region Garbage

//private void AddMotion(SimpleBody body)
//{
//    //TODO: Remove lastBody field
//    if (null == this.Body)
//    {
//        throw new Exception("Original body cannot be null");
//    }
//    if (null == this.BvhHierarchy)
//    {
//        throw new Exception("BvhHierachy property haven't been set (automatically or manually)");
//    }
//    var frame = new BvhFrame();
//    foreach (BvhJointType bvhJointType in BvhHierarchy.JointsMapping.Keys)
//    {
//        if (
//            this.BvhHierarchy.Joints[bvhJointType].MotionChannels.Contains(MotionChannelType.Xrotation) ||
//            this.BvhHierarchy.Joints[bvhJointType].MotionChannels.Contains(MotionChannelType.Yrotation) ||
//            this.BvhHierarchy.Joints[bvhJointType].MotionChannels.Contains(MotionChannelType.Zrotation)
//            )
//        {
//            var parent = this.BvhHierarchy.Joints[bvhJointType];
//            var parentMapped = BvhHierarchy.JointsMapping[bvhJointType];
//            if (!parentMapped.HasValue)
//            {
//                frame.AddMotion(bvhJointType, new EulerRotation(0, 0, 0));
//                continue;
//            }
//            var parentK = body.Joints[parentMapped.Value];

//            // This joint is leaf?
//            if (!BvhHierarchy.BvhJointRelationships.ContainsKey(bvhJointType))
//            {
//                frame.AddMotion(bvhJointType, new EulerRotation(0f, 0f, 0f));
//                continue;
//            }
//            // No, this joint has children
//            var tempList = new List<EulerRotation>();
//            foreach (BvhJointType childJointType in BvhHierarchy.BvhJointRelationships[bvhJointType])
//            {
//                var child = this.BvhHierarchy.Joints[childJointType];
//                Vector3 segment = new Vector3(child.OffsetX, child.OffsetY, child.OffsetZ);
//                var childMapped = BvhHierarchy.JointsMapping[childJointType];
//                if (!childMapped.HasValue)
//                {
//                    continue;
//                }
//                // Segment prime is the new bone orientation
//                var childKPrime = body.Joints[childMapped.Value];
//                Vector3 segmentPrime = Extensions.AdjustKinectCoords(
//                    childKPrime.Position.X - parentK.Position.X,
//                    childKPrime.Position.Y - parentK.Position.Y,
//                    childKPrime.Position.Z - parentK.Position.Z
//                    );
//                tempList.Add(EulerRotation.FromVectors(segment, segmentPrime));
//            }
//            if (tempList.Count == 0)
//            {
//                // No children joint, rotation always 0
//                frame.AddMotion(bvhJointType, new EulerRotation(0, 0, 0));
//            }
//            else
//            {
//                var rotation = new EulerRotation(0, 0, 0);
//                foreach (var r in tempList)
//                {
//                    rotation.Yaw += r.Yaw;
//                    rotation.Pitch += r.Pitch;
//                    rotation.Roll += r.Roll;
//                }
//                rotation.Yaw /= tempList.Count;
//                rotation.Pitch /= tempList.Count;
//                rotation.Roll /= tempList.Count;
//                frame.AddMotion(bvhJointType, rotation);
//            }
//        }

//        // Translation motion
//        if (this.BvhHierarchy.Joints[bvhJointType].MotionChannels.Contains(MotionChannelType.Xposition) ||
//            this.BvhHierarchy.Joints[bvhJointType].MotionChannels.Contains(MotionChannelType.Yposition) ||
//            this.BvhHierarchy.Joints[bvhJointType].MotionChannels.Contains(MotionChannelType.Zposition)
//            )
//        {
//            if (bvhJointType != BvhHierarchy.RootJoint)
//            {
//                throw new NotImplementedException("Translation motion channels are not implemented for joint other than ROOT yet.");
//            }
//            var rootK = body.Joints[BvhHierarchy.JointsMapping[BvhHierarchy.RootJoint].Value].Position;
//            var rootPrime = Extensions.AdjustKinectCoords(
//                rootK.X, rootK.Y, rootK.Z
//            ) * UNIT_SCALE;
//            frame.AddMotion(bvhJointType, rootPrime - this.BvhHierarchy.Reference);
//        }
//    }

//    this.frames.Add(frame);

//} 
#endregion