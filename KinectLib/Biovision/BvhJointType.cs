﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KinectLib.Biovision
{
    public enum BvhJointType
    {
        Hips,
        LeftUpLeg,
        LeftLeg,
        LeftFoot,
        LeftToeBase,
        RightUpLeg,
        RightLeg,
        RightFoot,
        RightToeBase,
        Spine,
        LeftShoulder,
        LeftArm,
        LeftForeArm,
        LeftHand,
        LeftFingerBase,
        RightShoulder,
        RightArm,
        RightForeArm,
        RightHand,
        RightFingerBase,
        Neck,
        Head
    }
}
