﻿using System;
using System.Diagnostics;

namespace KinectLib.Biovision
{
    // Represents a 3D vector.
    [Serializable]
    [DebuggerDisplay("X:{X} Y:{Y} Z:{Z}")]
    public struct Vector3Custom
    {

        public Vector3Custom(float x, float y, float z):this()
        {
            // TODO: Complete member initialization
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        public static bool operator ==(Vector3Custom a, Vector3Custom b)
        {
            return (a.X == b.X && a.Y == b.Y && a.Z == b.Z);
        }
        public static bool operator !=(Vector3Custom a, Vector3Custom b)
        {
            return !(a == b);
        }

        public static Vector3Custom operator +(Vector3Custom a, Vector3Custom b)
        {
            return new Vector3Custom(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }

        public static Vector3Custom operator /(Vector3Custom a, float b)
        {
            if (b == 0)
            {
                throw new DivideByZeroException();
            }
            return new Vector3Custom(a.X / b, a.Y / b, a.Z / b);
        }

        public static Vector3Custom operator -(Vector3Custom a, Vector3Custom b)
        {
            return new Vector3Custom(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }

        public static Vector3Custom operator *(Vector3Custom a, float b)
        {
            return new Vector3Custom(a.X * b, a.Y * b, a.Z * b);
        }


        //
        // Summary:
        //     The X coordinate of the vector.
        public float X { get; set; }

        //
        // Summary:
        //     The Y coordinate of the vector.
        public float Y { get; set; }

        // Summary:
        //     The W coordinate of the vector.
        public float Z { get; set; }

        // Summary:
        //     Returns a value that indicates whether this instance of is equal to a specified
        //     object.
        //
        // Parameters:
        //   obj:
        //     An object to compare with this instance.
        //
        // Returns:
        //     true if obj is equal to this instance; otherwise, false
        public override bool Equals(object obj)
        {
            if (obj is Vector3Custom)
            {
                return (Vector3Custom)obj == this;
            }
            else
            {
                return false;
            }
        }
        public bool Equals(Vector3Custom vector)
        {
            return vector == this;
        }

        public string ToBareString()
        {
            return string.Format("{0} {1} {2}", X, Y, Z);
        }
    }
}
