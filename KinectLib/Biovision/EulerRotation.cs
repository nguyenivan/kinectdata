﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using System.Diagnostics;

namespace KinectLib.Biovision
{
    [DebuggerDisplay("Yaw:{Yaw} Pitch:{Pitch} Roll:{Roll}")]
    public struct EulerRotation
    {
        public float Yaw;
        public float Pitch;
        public float Roll;

        public EulerRotation(float yaw, float pitch, float roll)
        {
            this.Yaw = yaw;
            this.Pitch = pitch;
            this.Roll = roll;
        }

        public override string ToString()
        {
            return string.Format("Yaw:{0} Pitch:{1} Roll:{2}", Yaw, Pitch, Roll);
        }

        public static EulerRotation FromVectors(OpenTK.Vector3 segment, OpenTK.Vector3 segmentPrime)
        {
            var n = Vector3.Cross(segment, segmentPrime);
            n.Normalize();
            var angle = Math.Acos(Vector3.Dot(segment, segmentPrime)
                / (segment.Length * segmentPrime.Length));
            var x = n.X;
            var y = n.Y;
            var z = n.Z;
            var s = Math.Sin(angle);
            var c = Math.Cos(angle);
            var t = 1 - c;
            //  if axis is not already normalised then uncomment this
            // double magnitude = Math.sqrt(x*x + y*y + z*z);
            // if (magnitude==0) throw error;
            // x /= magnitude;
            // y /= magnitude;
            // z /= magnitude;
            double psi = 0;
            double theta = 0;
            double phi = 0;
            if ((x * y * t + z * s) > 0.998)
            {
                // north pole singularity detected

                psi = 2 * Math.Atan2(x * Math.Sin(angle / 2), Math.Cos(angle / 2));
                theta = Math.PI / 2;
                phi = 0;
                return new EulerRotation((float)psi, (float)theta, (float)phi);
            }

            if ((x * y * t + z * s) < -0.998)
            {
                // south pole singularity detected
                psi = -2 * Math.Atan2(x * Math.Sin(angle / 2), Math.Cos(angle / 2));
                theta = -Math.PI / 2;
                phi = 0;
                return new EulerRotation((float)psi, (float)theta, (float)phi);
            }

            psi = Math.Atan2(y * s - x * z * t, 1 - (y * y + z * z) * t);
            theta = Math.Asin(x * y * t + z * s);
            phi = Math.Atan2(x * s - y * z * t, 1 - (x * x + z * z) * t);
            return new EulerRotation((float)psi, (float)theta, (float)phi);
        }

        /// <summary>
        /// (INCORRECT - REPLACED WITH FROM Q) Convert Quaternion rotation to Yaw Pitch ROll
        /// </summary>
        /// <param name="q1"></param>
        /// <returns>Y=yaw, X=pitch, Z=roll</returns>
        public static EulerRotation FromQuaternion(Quaternion q1)
        {
            double sqw = q1.W * q1.W;
            double sqx = q1.X * q1.X;
            double sqy = q1.Y * q1.Y;
            double sqz = q1.Z * q1.Z;
            double unit = sqx + sqy + sqz + sqw; // if normalised is one, otherwise is correction factor
            double test = q1.X * q1.Y + q1.Z * q1.W;
            var v = new EulerRotation();
            //Debug.Log("u: " + unit + ", test: " + test + ", test2: " + (q1.X * q1.Z + q1.Y * q1.W)
            //+ ", test3: " + (q1.X * q1.W + q1.Z * q1.Y));
            double psi = 0;
            double theta = 0;
            double phi = 0;
            if (test > 0.4999 * unit)
            { // singularity at north pole
                //Debug.Log("singularity1");
                psi = 2 * (float)Math.Atan2(q1.X, q1.W);
                theta = (float)Math.PI / 2;
                phi = 0;
                return new EulerRotation((float)psi, (float)theta, (float)phi);
            }
            if (test < -0.4999 * unit)
            { // singularity at south pole
                psi = -2 * (float)Math.Atan2(q1.X, q1.W);
                theta = -(float)Math.PI / 2;
                phi = 0;
                return new EulerRotation((float)psi, (float)theta, (float)phi);
            }
            Quaternion q = new Quaternion(q1.W, q1.Z, q1.X, q1.Y);
            psi = (float)Math.Atan2(2f * q.X * q.W + 2f * q.Y * q.Z, 1 - 2f * (q.Z * q.Z + q.W * q.W));     // Yaw
            theta = (float)Math.Asin(2f * (q.X * q.Z - q.W * q.Y));                             // Pitch
            phi = (float)Math.Atan2(2f * q.X * q.Y + 2f * q.Z * q.W, 1 - 2f * (q.Y * q.Y + q.Z * q.Z));      // Roll
            return new EulerRotation((float)psi, (float)theta, (float)phi);
        }//method 

        public static EulerRotation FromQ(Quaternion q)
        {
            var W = q.W;
            var X = q.X;
            var Y = q.Y;
            var Z = q.Z;
            float ww = W * W;
            float xx = X * X;
            float yy = Y * Y;
            float zz = Z * Z;
            float lengthSqd = xx + yy + zz + ww;
            float singularityTest = Y * W - X * Z;
            float singularityValue = Singularity * lengthSqd;
            return singularityTest > singularityValue
            ? new EulerRotation(-2 * (float)Math.Atan2(Z, W), 90.0f, 0.0f)
            : singularityTest < -singularityValue
            ? new EulerRotation(2 * (float)Math.Atan2(Z, W), -90.0f, 0.0f)
            : new EulerRotation((float)Math.Atan2(2.0f * (Y * Z + X * W), 1.0f - 2.0f * (xx + yy)),
            (float)Math.Asin(2.0f * singularityTest / lengthSqd),
            (float)Math.Atan2(2.0f * (X * Y + Z * W), 1.0f - 2.0f * (yy + zz)));
        }

        private const float Singularity = 0.499f;
    }
}
