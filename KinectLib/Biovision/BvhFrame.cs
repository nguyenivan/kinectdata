﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using OpenTK;
using Vector4 = OpenTK.Vector4;

namespace KinectLib.Biovision
{
    /// <summary>
    /// BVH Motion Frame
    /// </summary>
    class BvhFrame
    {
        /// <summary>
        /// These properties may be deleted after testing
        /// </summary>
        //public IDictionary<BvhJointType, Vector4?> OriginalOrientation { get; private set; }
        //public IDictionary<BvhJointType, Vector4?> CurrentOrientation { get; private set; }
        //public IDictionary<BvhJointType, Vector3?> OriginalPosition { get; private set; }
        //public IDictionary<BvhJointType, Vector3?> CurrentPosition { get; private set; }

        /// <summary>
        /// Montion information
        /// </summary>
        public IDictionary<MotionChannelType, IDictionary<BvhJointType, float>> MotionChannels
        {
            get;
            private set;
        }

        public BvhFrame()
        {
            this.MotionChannels = new Dictionary<MotionChannelType, IDictionary<BvhJointType, float>>();
            foreach (MotionChannelType channel in Enum.GetValues(typeof(MotionChannelType)))
            {
                this.MotionChannels[channel] = new Dictionary<BvhJointType, float>();
            }
            //this.OriginalOrientation = new Dictionary<BvhJointType, Vector4?>();
            //this.CurrentOrientation = new Dictionary<BvhJointType, Vector4?>();
            //this.OriginalPosition = new Dictionary<BvhJointType, Vector3?>();
            //this.CurrentPosition = new Dictionary<BvhJointType, Vector3?>();
        }

        ///// <summary>
        ///// Add motion in rotation channels
        ///// </summary>
        ///// <param name="lastOrientation">Last quaternion</param>
        ///// <param name="currentOrientaion">Current quaternion</param>
        //public void AddMotion(BvhJointType bvhJointType, Vector4? lastOrientationN, Vector4? currentOrientationN)
        //{
        //    if (currentOrientationN.HasValue)
        //    {
        //        var currentOrientation = currentOrientationN.Value;
        //        this.CurrentOrientation[bvhJointType] = currentOrientation;
        //        if (lastOrientationN.HasValue)
        //        {
        //            var lastOrientation = lastOrientationN.Value;
        //            this.OriginalOrientation[bvhJointType] = lastOrientation;
        //            Quaternion q1 = new Quaternion(lastOrientation.X, lastOrientation.Y, lastOrientation.Z, lastOrientation.W);
        //            q1.Invert();
        //            Quaternion q2 = new Quaternion(currentOrientation.X, currentOrientation.Y, currentOrientation.Z, currentOrientation.W);
        //            Quaternion diff = q2 * q1;
        //            Vector3 motion = diff.ToYawPitchRoll();
        //            if (float.IsNaN(motion.X))
        //            {
        //                motion = new Vector3(0, 0, 0);
        //            }
        //            this.MotionChannels[MotionChannelType.Xrotation][bvhJointType] = -motion.X;
        //            this.MotionChannels[MotionChannelType.Yrotation][bvhJointType] = motion.Y;
        //            this.MotionChannels[MotionChannelType.Zrotation][bvhJointType] = -motion.Z;
        //            return;
        //        }
        //    }
        //    this.MotionChannels[MotionChannelType.Xrotation][bvhJointType] = 0;
        //    this.MotionChannels[MotionChannelType.Yrotation][bvhJointType] = 0;
        //    this.MotionChannels[MotionChannelType.Zrotation][bvhJointType] = 0;
        //    //TODO: Unit test the Quaternion to YawPitchRoll method
        //}

        /// <summary>
        /// Add motion in translate channels
        /// </summary>
        /// <param name="lastPosition">Last Vector3</param>
        /// <param name="currentPosition">Current Vector3</param>
        //public void AddMotion(BvhJointType bvhJointType, Vector3? lastPositionN, Vector3? currentPositionN)
        //{
        //    if (null != currentPositionN)
        //    {
        //        var currentPosition = currentPositionN.Value;
        //        this.CurrentPosition[bvhJointType] = currentPosition;
        //        if (null != lastPositionN)
        //        {
        //            var lastPosition = lastPositionN.Value;
        //            this.OriginalPosition[bvhJointType] = lastPosition;
        //            Vector3 motion = currentPosition - lastPosition;
        //            this.MotionChannels[MotionChannelType.Xposition][bvhJointType] = -motion.X * Bvh.UNIT_SCALE;
        //            this.MotionChannels[MotionChannelType.Yposition][bvhJointType] = motion.Y * Bvh.UNIT_SCALE;
        //            this.MotionChannels[MotionChannelType.Zposition][bvhJointType] = -motion.Z * Bvh.UNIT_SCALE;
        //            return;
        //        }
        //    }
        //    this.MotionChannels[MotionChannelType.Xposition][bvhJointType] = 0;
        //    this.MotionChannels[MotionChannelType.Yposition][bvhJointType] = 0;
        //    this.MotionChannels[MotionChannelType.Zposition][bvhJointType] = 0;
        //}

        internal void AddMotion(BvhJointType bvhJointType, EulerRotation rotation)
        {
            
            this.MotionChannels[MotionChannelType.Xrotation][bvhJointType] = (float)Math.Round(MathHelper.RadiansToDegrees(rotation.Yaw ));
            this.MotionChannels[MotionChannelType.Yrotation][bvhJointType] = (float)Math.Round(MathHelper.RadiansToDegrees(rotation.Pitch));
            this.MotionChannels[MotionChannelType.Zrotation][bvhJointType] = (float)Math.Round(MathHelper.RadiansToDegrees(rotation.Roll));
        }

        public void AddMotion(BvhJointType bvhJointType, Vector3 position)
        {
            this.MotionChannels[MotionChannelType.Xposition][bvhJointType] = (float)Math.Round(position.X);
            this.MotionChannels[MotionChannelType.Yposition][bvhJointType] = (float)Math.Round(position.Y);
            this.MotionChannels[MotionChannelType.Zposition][bvhJointType] = (float)Math.Round(position.Z);
        }
    }
}
