﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KinectLib.Biovision
{
    public enum MotionChannelType
    {
        Xposition,
        Yposition,
        Zposition,
        Xrotation,
        Yrotation,
        Zrotation
    }
}
