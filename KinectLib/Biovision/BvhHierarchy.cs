﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Kinect;
using OpenTK;

namespace KinectLib.Biovision
{

    public enum BvhType
    {
        Biped
    }

    /// <summary>
    /// Biped template for Bvh Hierarchy
    /// </summary>
    public class BvhHierarchy
    {
        private Dictionary<BvhJointType, BvhJoint> joints = new Dictionary<BvhJointType, BvhJoint>();
        private Dictionary<BvhJointType, Vector3> endSiteOffsets = new Dictionary<BvhJointType, Vector3>();

        #region BIPED Template Stuff
        public static readonly Dictionary<BvhJointType, List<BvhJointType>> BvhJointRelationships = new Dictionary<BvhJointType, List<BvhJointType>>()
        {
            {BvhJointType.Hips,new List<BvhJointType>(){BvhJointType.Spine,BvhJointType.LeftUpLeg,BvhJointType.RightUpLeg}},
            {BvhJointType.LeftUpLeg,new List<BvhJointType>(){BvhJointType.LeftLeg}},
            {BvhJointType.LeftLeg,new List<BvhJointType>(){BvhJointType.LeftFoot}},
            {BvhJointType.LeftFoot,new List<BvhJointType>(){BvhJointType.LeftToeBase}},
            {BvhJointType.RightUpLeg,new List<BvhJointType>(){BvhJointType.RightLeg}},
            {BvhJointType.RightLeg,new List<BvhJointType>(){BvhJointType.RightFoot}},
            {BvhJointType.RightFoot,new List<BvhJointType>(){BvhJointType.RightToeBase}},
            {BvhJointType.Spine,new List<BvhJointType>(){BvhJointType.Neck,BvhJointType.LeftShoulder,BvhJointType.RightShoulder}},
            {BvhJointType.LeftShoulder,new List<BvhJointType>(){BvhJointType.LeftArm}},
            {BvhJointType.LeftArm,new List<BvhJointType>(){BvhJointType.LeftForeArm}},
            {BvhJointType.LeftForeArm,new List<BvhJointType>(){BvhJointType.LeftHand}},
            {BvhJointType.LeftHand,new List<BvhJointType>(){BvhJointType.LeftFingerBase}},
            {BvhJointType.RightShoulder,new List<BvhJointType>(){BvhJointType.RightArm}},
            {BvhJointType.RightArm,new List<BvhJointType>(){BvhJointType.RightForeArm}},
            {BvhJointType.RightForeArm,new List<BvhJointType>(){BvhJointType.RightHand}},
            {BvhJointType.RightHand,new List<BvhJointType>(){BvhJointType.RightFingerBase}},
            {BvhJointType.Neck,new List<BvhJointType>(){BvhJointType.Head}}

        };

        public static readonly Dictionary<JointType, List<JointType>> JointRelationships = new Dictionary<JointType, List<JointType>>()
        {
            {JointType.SpineBase,new List<JointType>(){JointType.SpineMid,JointType.HipLeft,JointType.HipRight}},
            {JointType.SpineMid,new List<JointType>(){JointType.SpineShoulder}},
            {JointType.SpineShoulder,new List<JointType>(){JointType.Neck, JointType.ShoulderLeft,JointType.ShoulderRight}},
            {JointType.Neck,new List<JointType>(){JointType.Head}},
            {JointType.ShoulderLeft,new List<JointType>(){JointType.ElbowLeft}},
            {JointType.ElbowLeft,new List<JointType>(){JointType.WristLeft}},
            {JointType.WristLeft,new List<JointType>(){JointType.HandLeft}},
            {JointType.HandLeft,new List<JointType>(){JointType.HandTipLeft,JointType.ThumbLeft}},
            {JointType.ShoulderRight,new List<JointType>(){JointType.ElbowRight}},
            {JointType.ElbowRight,new List<JointType>(){JointType.WristRight}},
            {JointType.WristRight,new List<JointType>(){JointType.HandRight}},
            {JointType.HandRight,new List<JointType>(){JointType.HandTipRight,JointType.ThumbRight}},
            {JointType.HipLeft,new List<JointType>(){JointType.KneeLeft}},
            {JointType.KneeLeft,new List<JointType>(){JointType.AnkleLeft}},
            {JointType.AnkleLeft,new List<JointType>(){JointType.FootLeft}},
            {JointType.HipRight,new List<JointType>(){JointType.KneeRight}},
            {JointType.KneeRight,new List<JointType>(){JointType.AnkleRight}},
            {JointType.AnkleRight,new List<JointType>(){JointType.FootRight}}
        };

        public static readonly Dictionary<JointType, JointType?> OrientationsSource = new Dictionary<JointType, JointType?>()
        {
            {JointType.SpineBase,JointType.SpineBase},
            {JointType.SpineMid,JointType.SpineBase},
            {JointType.SpineShoulder,JointType.SpineMid},
            {JointType.Neck,JointType.SpineShoulder},
            {JointType.Head,null},
            {JointType.ShoulderLeft,JointType.ShoulderLeft},
            {JointType.ElbowLeft,JointType.ElbowLeft},
            {JointType.WristLeft,JointType.WristLeft},
            {JointType.HandLeft,JointType.HandLeft},
            {JointType.HandTipLeft,null},
            {JointType.ShoulderRight,JointType.ShoulderRight},
            {JointType.ElbowRight,JointType.ElbowRight},
            {JointType.WristRight,JointType.WristRight},
            {JointType.HandRight,JointType.HandRight},
            {JointType.HandTipRight,null},
            {JointType.HipLeft,JointType.HipLeft},
            {JointType.KneeLeft,JointType.KneeLeft},
            {JointType.AnkleLeft,JointType.AnkleLeft},
            {JointType.FootLeft,null},
            {JointType.HipRight,JointType.HipRight},
            {JointType.KneeRight,JointType.KneeRight},
            {JointType.AnkleRight,JointType.AnkleRight},
            {JointType.FootRight,null},
            {JointType.ThumbLeft,null},
            {JointType.ThumbRight,null}

        };

        public static readonly Dictionary<BvhJointType, Microsoft.Kinect.JointType?> JointsMapping = new Dictionary<BvhJointType, Microsoft.Kinect.JointType?>() 
        {
            {BvhJointType.Hips, JointType.SpineBase},
            {BvhJointType.LeftUpLeg, JointType.HipLeft},
            {BvhJointType.LeftLeg, JointType.KneeLeft},
            {BvhJointType.LeftFoot, JointType.AnkleLeft},
            {BvhJointType.LeftToeBase, JointType.FootLeft},
            {BvhJointType.RightUpLeg, JointType.HipRight},
            {BvhJointType.RightLeg, JointType.KneeRight},
            {BvhJointType.RightFoot, JointType.AnkleRight},
            {BvhJointType.RightToeBase, JointType.FootRight},
            {BvhJointType.Spine, JointType.SpineMid},
            {BvhJointType.LeftShoulder, JointType.SpineShoulder},
            {BvhJointType.LeftArm, JointType.ShoulderLeft},
            {BvhJointType.LeftForeArm, JointType.ElbowLeft},
            {BvhJointType.LeftHand, JointType.WristLeft},
            {BvhJointType.LeftFingerBase, JointType.HandLeft},
            {BvhJointType.RightShoulder, JointType.SpineShoulder},
            {BvhJointType.RightArm, JointType.ShoulderRight},
            {BvhJointType.RightForeArm, JointType.ElbowRight},
            {BvhJointType.RightHand, JointType.WristRight},
            {BvhJointType.RightFingerBase, JointType.HandRight},
            {BvhJointType.Neck, JointType.Neck},
            {BvhJointType.Head, JointType.Head}
        };

        public static readonly BvhJointType RootJoint = BvhJointType.Hips;
        public static readonly JointType KinectRootJoint = JointsMapping[RootJoint].Value;

        private static readonly MotionChannelType[] FullChannels = new MotionChannelType[] 
        { 
            MotionChannelType.Xposition,
            MotionChannelType.Yposition,
            MotionChannelType.Zposition,
            MotionChannelType.Xrotation,
            MotionChannelType.Yrotation,
            MotionChannelType.Zrotation
        };

        private static readonly MotionChannelType[] RotateOnlyChannels = new MotionChannelType[] 
        { 
            MotionChannelType.Xrotation,
            MotionChannelType.Yrotation,
            MotionChannelType.Zrotation
        };
        #endregion

        public BvhHierarchy(BvhType template = BvhType.Biped)
        {
            ///TODO: Check the integrity of joints hierarchy
            if (template != BvhType.Biped)
            {
                throw new NotImplementedException(
                      string.Format("This bvh template is not implemented: {0}", template));
            }

            foreach (KeyValuePair<BvhJointType, JointType?> entry in JointsMapping)
            {
                BvhJoint joint = null;
                this.joints.TryGetValue(entry.Key, out joint);
                // If the join type is not in joints dictionary yet, add it
                if (null == joint)
                {
                    MotionChannelType[] channels = RootJoint == entry.Key ? FullChannels : RotateOnlyChannels;
                    this.joints.Add(entry.Key, new BvhJoint(entry.Key, channels));
                }
            }

        }

        public IDictionary<BvhJointType, BvhJoint> Joints
        {
            get { return this.joints; }
        }

        public float[][] ExportVectors()
        {
            // Matrix to return
            var m = new float[BvhHierarchy.JointsMapping.Count][];
            var points = new Dictionary<BvhJointType, Vector3>();
            var root = GetJointOffset(RootJoint);
            points.Add(RootJoint, root);

            m[0] = new float[] { 0, 0, 0, root.X, root.Y, root.Z };
            int i = 1;
            foreach (BvhJointType bvhJointType in Traverse())
            {
                if (bvhJointType == RootJoint) continue;
                var parent = FindParent(bvhJointType, false);
                if (!points.ContainsKey(parent))
                {
                    throw new Exception(
                      "Coordinates of parent joint haven't beeen calculated before child joint's. " +
                      "Possible traverse algorithm is not correct.");
                }
                points.Add(bvhJointType, points[parent] + GetJointOffset(bvhJointType));
                var start = points[parent];
                var end = points[bvhJointType];
                m[i] = new float[]{
                    start.X,
                    start.Y,
                    start.Z,
                    end.X,
                    end.Y,
                    end.Z
                };
                i++;
            }
            return m;

        }


        /// <summary>
        /// Traverse the joint tree
        /// </summary>
        /// <returns>IEnumerable for BvhJointType (parent before children)</returns>
        public static IEnumerable<BvhJointType> Traverse()
        {
            var stack = new Stack<BvhJointType>();
            stack.Push(RootJoint);
            while (stack.Count > 0)
            {
                var current = stack.Pop();
                yield return current;
                if (BvhJointRelationships.ContainsKey(current))
                {
                    foreach (var child in BvhJointRelationships[current])
                        stack.Push(child);
                }
            }
        }

        /// <summary>
        /// Set offset xyz for a joint in the hierarchy
        /// </summary>
        /// <param name="bvhJointType">BVH joint type</param>
        /// <param name="x">Offset X</param>
        /// <param name="y">Offset Y</param>
        /// <param name="z">Offset Z</param>
        public void SetJointOffset(BvhJointType bvhJointType, float x, float y, float z)
        {
            if (!this.joints.ContainsKey(bvhJointType))
            {
                throw new ArgumentException(string.Format("Joint type is not in hierarchy: {0}", bvhJointType));
            }
            this.joints[bvhJointType].SetOffset(x, y, z);
        }

        /// <summary>
        /// Set offset for a Bvh's End Site joint
        /// </summary>
        /// <param name="bvhJointType">BVH joint type</param>
        /// <param name="offset">Offset in Vector3</param>
        public void SetEndSiteOffset(BvhJointType bvhJointType, Vector3 offset)
        {
            this.endSiteOffsets[bvhJointType] = offset;
        }

        /// <summary>
        /// Set offset for a Bvh's End Site joint
        /// </summary>
        /// <param name="bvhJointType">BVH joint type</param>
        /// <returns>Offset of the End Site joint</returns>
        public Vector3 GetEndSiteOffset(BvhJointType bvhJointType)
        {
            if (this.endSiteOffsets.ContainsKey(bvhJointType))
            {
                return this.endSiteOffsets[bvhJointType];
            }
            else
            {
                return new Vector3();
            }
        }

        public IDictionary<BvhJointType, Vector3> GetAllEndSiteOffsets()
        {
            return JointsMapping.ToDictionary(x => x.Key, x => GetEndSiteOffset(x.Key));
        }
        /// <summary>
        /// Set offset xyz for a joint in the hierarchy
        /// </summary>
        /// <param name="bvhJointType">BVH joint type</param>
        /// <param name="offset">Offset vector3</param>
        public void SetJointOffset(BvhJointType bvhJointType, Vector3 offset)
        {
            SetJointOffset(bvhJointType, offset.X, offset.Y, offset.Z);
        }

        /// <summary>
        /// Set offset xyz for a joint in the hierarchy
        /// </summary>
        /// <param name="bvhJointType">BVH joint type</param>
        /// <returns>Offset xyz of the joint</returns>
        public Vector3 GetJointOffset(BvhJointType bvhJointType)
        {
            if (!this.joints.ContainsKey(bvhJointType))
            {
                throw new ArgumentException(string.Format("Joint type is not in hierarchy: {0}", bvhJointType));
            }
            Vector3 offset = new Vector3(
                 this.joints[bvhJointType].OffsetX,
                 this.joints[bvhJointType].OffsetY,
                 this.joints[bvhJointType].OffsetZ
            );
            return offset;
        }

        public IDictionary<BvhJointType, Vector3> GetAllJointOffsets()
        {
            return JointsMapping.ToDictionary(x => x.Key, x => GetJointOffset(x.Key));
        }

        /// <summary>
        /// Find the nearest ancestor BvhJointType that has correspondent Kinect joint type
        /// </summary>
        /// <param name="child"></param>
        /// <param name="requireKinect"></param>
        /// <returns></returns>
        public static BvhJointType FindParent(BvhJointType child, bool requireKinect = true)
        {
            BvhJointType current = child;
            while (current != RootJoint)
            {
                var parents = BvhJointRelationships
                    .Where(x => x.Value.Contains(current))
                    .Select(x => x.Key);
                if (parents.Count() > 1)
                {
                    throw new Exception(
                        string.Format("Multiple parent joints found for: {0}", current));
                }
                else if (parents.Count() < 1)
                {
                    throw new Exception(
                        string.Format("No parent joint found for: {0}", current));
                }
                else
                {
                    var parent = parents.ToList()[0];
                    if (!requireKinect || null != JointsMapping[parent])
                    {
                        return parent;
                    }
                    else
                    {
                        current = parent;
                    }
                }
            }
            throw new Exception("Cannot find parent of root joint.");
        }

        public Vector3 Reference { get; set; }

        public static JointType FindParent(JointType jointType)
        {
            try
            {
                var m = JointsMapping.First(x => x.Value.HasValue && x.Value.Value == jointType);
                return BvhHierarchy.JointsMapping[FindParent(m.Key)].Value;
            }
            catch (InvalidOperationException)
            {
                throw new Exception(string.Format("Cannot find the correspondent joint type for joint {0}", jointType));
            }
        }
    }
}
