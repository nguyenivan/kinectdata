﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KinectLib.Biovision
{
    public class BvhJoint
    {
        public BvhJointType BvhJointType { get; set; }

        public MotionChannelType[] MotionChannels { get; private set; }

        public float OffsetX { get; set; }
        public float OffsetY { get; set; }
        public float OffsetZ { get; set; }

        public BvhJoint(BvhJointType bvhJointType, MotionChannelType[] motionChannels)
        {
            this.BvhJointType = bvhJointType;
            this.MotionChannels = motionChannels;
        }

        public void SetOffset(float x, float y, float z)
        {
            this.OffsetX = x;
            this.OffsetY = y;
            this.OffsetZ = z;
        }
    }
}
