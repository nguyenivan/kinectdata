﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectLib
{
    public delegate void PoseEventHandler(System.Object sender, PoseEventArgs e);
    public class PoseEventArgs : EventArgs
    {
        public PoseEventArgs(PoseType pose)
        {
            this.Pose = pose;
        }
        public PoseType Pose { get; set; }
    }
}
