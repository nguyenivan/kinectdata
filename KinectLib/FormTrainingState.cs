﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KinectLib
{
    public enum FormTrainingState
    {
        Paused,
        Running,
        Finished
    }
}
