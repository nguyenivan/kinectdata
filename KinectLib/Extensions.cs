﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;
using System.Media;
using System.Reflection;
using System.Windows.Media.Imaging;
using System.Collections.Concurrent;
using System.Linq.Expressions;
using KinectLib.Biovision;
using OpenTK;
using Vector4 = Microsoft.Kinect.Vector4;
using System.Collections.ObjectModel;
using System.Resources;

namespace KinectLib
{
    public static class Extensions
    {
        private static readonly Destructor Finalizer = new Destructor();

        private sealed class Destructor
        {
            ~Destructor()
            {
                soundPlayer.Dispose();
            }
        }


        //private static readonly IDictionary<PoseType, string> _VN_POSE_NAMES = 
        //    new ReadOnlyDictionary<PoseType,string>( new Dictionary<PoseType,string>(){
        //        {PoseType.Unknown,"Unknown"},
        //        {PoseType.DungThang,"Đứng Thẳng"},
        //        {PoseType.LapTan,"Lập Tấn"},
        //        {PoseType.VanVo,"Văn Võ"},
        //        {PoseType.TuBinh,"Tứ Bình"},
        //        {PoseType.HoTraoTrai,"Hổ Trảo Trái"},
        //        {PoseType.ThoiSonPhai,"Thôi Sơn Phải"},
        //        {PoseType.HoiChoPhai,"Hồi Chỏ Phải"},
        //        {PoseType.TramKieuPhai,"Trầm Kiều Phải"},
        //        {PoseType.HoTraoPhai,"Hổ Trảo Phải"},
        //        {PoseType.ThoiSonTrai,"Thôi Sơn Trái"},
        //        {PoseType.HoiChoTrai,"Hồi Chỏ Trái"},
        //        {PoseType.TramKieuTrai,"Trầm Kiều Trái"},
        //        {PoseType.QuaiChuyTrai,"Quải Chùy Trái"}
        //    });

        private static readonly ResourceManager RES = new ResourceManager(
            "KinectLib.Resources.FormOne", typeof(KinectLib.Extensions).Assembly);
        /// <summary>
        /// Get a friendly name for this pose. To be implement: Culture
        /// </summary>
        /// <param name="pose">The pose</param>
        /// <returns></returns>
        public static string GetName(this PoseType pose)
        {
            return RES.GetString(pose.ToString());
        }

        /// <summary>
        /// With the above extension you can write this:
        /// var str = "{foo} {bar} {baz}".Format(foo=>foo, bar=>2, baz=>new object());
        /// and you'll get "foo 2 System.Object".
        /// </summary>
        /// <param name="str">Format string</param>
        /// <param name="args">Arguments</param>
        /// <returns></returns>
        public static string Format(this string str, params Expression<Func<string, object>>[] args)
        {
            var parameters = args.ToDictionary
                                (e => string.Format("{{{0}}}", e.Parameters[0].Name),
                                e => e.Compile()(e.Parameters[0].Name));
            var sb = new StringBuilder(str);
            foreach (var kv in parameters)
            {
                sb.Replace(kv.Key
                          , kv.Value != null ? kv.Value.ToString() : "");
            }
            return sb.ToString();
        }

        /// <summary>
        /// Try to enqueue a thread-safe concurrent queue and make sure the queue length not exceed the limit
        /// </summary>
        /// <typeparam name="T">Type of queue</typeparam>
        /// <param name="queue">The queue</param>
        /// <param name="obj">object to enqueue</param>
        /// <param name="limit">Limit of queue size</param>
        /// <returns>True if queue is full</returns>
        public static bool EnqueueOverflow<T>(this ConcurrentQueue<T> queue, T obj, int limit)
        {
            queue.Enqueue(obj);
            lock (queue)
            {
                T overflow;
                while (queue.Count > limit && queue.TryDequeue(out overflow)) ;
                return queue.Count >= limit;
            }
        }

        public static void Clear<T>(this ConcurrentQueue<T> queue)
        {
            T item;
            while (queue.TryDequeue(out item))
            {
                // do nothing
            }
        }


        /// <summary>
        /// Convert joint orientation (Vector4) to a Quaterion and mirror X and Z
        /// </summary>
        /// <param name="orientation">Vector4 orientaion</param>
        /// <returns>Equivalent quaternion</returns>
        public static Quaternion ToQuaternion(this Vector4 orientation)
        {
            var retVal = new Quaternion(orientation.X, orientation.Y, orientation.Z, orientation.W);
            return retVal;
        }

        public static float Rad2Deg = (180 / (float)Math.PI);

        public static string ToBareString(this Vector3 v)
        {
            return string.Format("{0} {1} {2}", v.X, v.Y, v.Z);
        }

        public static void NormalizeAngles(this double[] vector)
        {
            for (int i = 0; i < vector.Length; i++)
            {
                vector[i] = vector[i] / 180;
            }
        }

        private static readonly SoundPlayer soundPlayer =
            new SoundPlayer(Assembly.GetExecutingAssembly().GetManifestResourceStream("KinectLib.Resources.beep.wav"));

        public static void Beep()
        {
            soundPlayer.Play();
        }

        /* List of Angles need to be compared
         * 1. head - sh. center - left sh.
         * 2. head - sh. center - right sh.
         * 3. left sh. - sh. center - right sh.
         * 4. sh. center - right sh. - elbow right
         * 5. right sh. - elbow right - wrist right
         * 6. elbow right - wrist right - hand right
         */


        public static Quaternion DiffQuaternion(SimpleBody originalBody, SimpleBody body, JointType jointType)
        {
            if (jointType != JointType.SpineBase)
            {
                var parentType = BvhHierarchy.FindParent(jointType);
                var parent = originalBody.JointOrientations[parentType].Orientation.ToQuaternion();
                var parentPrime = body.JointOrientations[parentType].Orientation.ToQuaternion();
                var child = originalBody.JointOrientations[jointType].Orientation.ToQuaternion();
                var childPrime = body.JointOrientations[jointType].Orientation.ToQuaternion();
                var childDiff = DiffQuaterion(parent, child, parentPrime, childPrime);
                return childDiff;
            }
            else
            {
                var parent = Quaternion.Identity;
                var parentPrime = Quaternion.Identity;
                var child = originalBody.JointOrientations[jointType].Orientation.ToQuaternion();
                var childPrime = body.JointOrientations[jointType].Orientation.ToQuaternion();
                var childDiff = DiffQuaterion(parent, child, parentPrime, childPrime);
                return childDiff;
            }

        }

        /// <summary>
        /// Let P=Parent bone, C = Child bone
        /// Algorithm:
        /// qPdiff = qP*invert(qPprime) //e.g. quat(Pprime -> P); qCprime_revert = qPdiff * qCprime;
        /// qCdiff = qCprime_revert * invert(qC); [x y z] = getEuler(qCdiff,'xyz');
        /// </summary>
        /// <param name="parent">Parent quaternion</param>
        /// <param name="child">Child quaternion</param>
        /// <param name="parentPrime">Parent Prime quaternion</param>
        /// <param name="childPrime">Child Prime quaternion</param>
        /// <returns>The diff quaterion to rotate the child joint before applying parent rotation</returns>
        public static Quaternion DiffQuaterion(Quaternion parent, Quaternion child, Quaternion parentPrime, Quaternion childPrime)
        {
            var parentDiff = parent.AdjustKinectCoords() * Quaternion.Invert(parentPrime.AdjustKinectCoords());
            var childPrimeRev = parentDiff * childPrime.AdjustKinectCoords();
            var childDiff = childPrimeRev * Quaternion.Invert(child.AdjustKinectCoords());
            return childDiff;
        }



        /// <summary>
        /// Calculate local joint orientation, because the Vector4 supplied by Kinect SDK is global
        /// </summary>
        /// <param name="originalBody"></param>
        /// <param name="body"></param>
        /// <param name="jointType"></param>
        /// <returns></returns>
        public static Quaternion LocalQuaternion(SimpleBody body, JointType jointType)
        {
            var q = body.JointOrientations[jointType].Orientation.ToQuaternion();
            // If this is root joint, the orientation is global and local at the same time
            if (jointType == BvhHierarchy.JointsMapping[BvhHierarchy.RootJoint].Value)
            {
                return q;
            }
            // Calculate local orientation
            var parentType = BvhHierarchy.FindParent(jointType);
            var qParent = body.JointOrientations[parentType].Orientation.ToQuaternion();
            var qParentInverted = Quaternion.Invert(qParent);
            return q * qParentInverted;
        }


        /// <summary>
        /// Calculate natural logarithm of a quaternion
        /// returns the logarithm of a quaternion = v*a where q = [cos(a),v*sin(a)]
        /// </summary>
        /// <param name="q">Input quaternion</param>
        /// <returns>The result quaternion</returns>
        public static Quaternion Log(this Quaternion q)
        {
            Vector3 v;
            float theta;
            q.ToAxisAngle(out v, out theta);
            float a = (float)Math.Acos(theta);
            float sina = (float)Math.Sin(a);
            Vector3 axis;
            if (sina > 0)
            {
                axis.X = a * v.X / sina;
                axis.Y = a * v.Y / sina;
                axis.Z = a * v.Z / sina;
            }
            else
            {
                axis.X = axis.Y = axis.Z = 0;
            }
            return Quaternion.FromAxisAngle(axis, 0f);
        }

        /// <summary>
        /// Calculate natural exponential of a quaternion
        /// returns e^quaternion = exp(v*a) = [cos(a),vsin(a)]
        /// </summary>
        /// <param name="q">Input quaternion</param>
        /// <returns>The result quaternion</returns>
        public static Quaternion Exp(this Quaternion q)
        {
            Vector3 v;
            float theta;
            q.ToAxisAngle(out v, out theta);
            float a = (float)v.Length;
            float sina = (float)Math.Sin(a);
            float cosa = (float)Math.Cos(a);

            float angle = cosa;
            Vector3 axis;
            if (a > 0)
            {
                axis.X = sina * v.X / a;
                axis.Y = sina * v.Y / a;
                axis.Z = sina * v.Z / a;
            }
            else
            {
                axis.X = axis.Y = axis.Z = 0;
            }
            return Quaternion.FromAxisAngle(axis, angle);
        }


        /// <summary>
        /// Calculate mean (average) of multiple quaternions
        /// </summary>
        /// <see cref="http://en.wikipedia.org/wiki/Generalized_quaternion_interpolation"/>
        /// <param name="quaternions">IEnumerable of quaternions</param>
        /// <returns>The mean quaternion</returns>
        public static Quaternion MeanQuaternion(IEnumerable<Quaternion> quaternions)
        {
            //Quaternion eLast = new Quaternion(0,0,0,0);
            //Quaternion mLast = quaternions.ElementAt(0);
            //for (int i = 0; i < quaternions.Count(); i++)
            //{
            //    var e = new Quaternion(0,0,0,0);
            //    var mInvert = Quaternion.Invert(mLast);
            //    for (var j = 0; j < i; j++)
            //    {
            //        e += (mInvert * quaternions.ElementAt(j)).Log();
            //    }
            //    var m = mLast * eLast.Exp();
            //    eLast = e;
            //    mLast = m;
            //}
            //return mLast;
            var sumQuat = new Quaternion();
            foreach (var q in quaternions)
            {
                sumQuat = sumQuat + q.GetPositive();
            }
            sumQuat.Normalize();
            return sumQuat;
        }

        /// <summary>
        /// Return a quaternion that always has a positive W component
        /// </summary>
        /// <param name="q">Input quaternion</param>
        /// <returns>Quaternion with a positive W component</returns>
        public static Quaternion GetPositive(this Quaternion q)
        {
            if (0 < q.W)
            {
                return q;
            }
            else
            {
                return q * -1;
            }
        }

        public static string MeaningfulDisplay(this Quaternion q, bool wIsLast = true)
        {
            if (wIsLast)
            {
                return string.Format("{0} {1} {2} {3}", q.X, q.Y, q.Z, q.W);
            }
            else
            {
                return string.Format("{0} {1} {2} {3}", q.W, q.X, q.Y, q.Z);
            }
        }

        public static string MeaningfulDisplay(this CameraSpacePoint v)
        {
            return string.Format("{0} {1} {2}", v.X, v.Y, v.Z);
        }
        public static string MeaningfulDisplay(this EulerRotation e)
        {
            return string.Format("{0} {1} {2}", e.Yaw, e.Pitch, e.Roll);
        }

        /// <summary>
        /// Negate X and Z
        /// </summary>
        /// <param name="vector3">Vector3 in Kinect system</param>
        public static Vector3 AdjustKinectCoords(float x, float y, float z)
        {
            return new Vector3(-x, y, -z);
        }

        public static Quaternion AdjustKinectCoords(this Quaternion q)
        {
            return new Quaternion(-q.X, q.Y, -q.Z, q.W);
        }

        public static OpenTK.Matrix4 CameraTransform(Vector3 normal, float scalar)
        {
            // Note the first three coefficients of the plane equation are the normal
            Vector3 yNew = new Vector3(normal.X, normal.Y, normal.Z);
            Vector3 zNew = new Vector3(0, 1, -normal.Y / normal.Z);
            zNew.Normalize();
            Vector3 xNew = Vector3.Cross(yNew, zNew);

            // assumes column vectors, multiplied on the right
            var rotMatrix = new OpenTK.Matrix4(
                xNew.X, yNew.X, zNew.X, 0,
                xNew.Y, yNew.Y, zNew.Y, 0,
                xNew.Z, yNew.Z, zNew.Z, 0,
                0, 0, 0, 1);

            var translationMatrix = OpenTK.Matrix4.CreateTranslation(0, scalar, 0);

            return rotMatrix * translationMatrix;
        }

    }
}
