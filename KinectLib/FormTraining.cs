﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectLib
{
    public class FormTraining
    {

        public static readonly int TEMPORAL_DATA_LENGTH = 10000;
        public static readonly double DEFAULT_ENTER_POSE_RATE = 0.5;
        public static readonly int DEFAULT_BUFFER_LENGTH = 20;
        public event PoseEventHandler OnMatch = delegate { };
        public event PoseEventHandler OnProceed = delegate { };
        public event EventHandler OnStart = delegate { };
        public event EventHandler OnFinish = delegate { };
        public event EventHandler OnPause = delegate { };
        public event EventHandler OnStop = delegate { };

        private FixedSizedQueue<SimpleBody> bodyBuffer;
        private FixedSizedQueue<PoseType> buffer;
        private int _bufferLength;
        private PoseMatcher poseMatcher;

        /// <summary>
        /// Initialize new form with a name
        /// </summary>
        /// <param name="name">name of the form</param>
        public FormTraining(PowerForm name)
        {
            this.Name = name;
            this.poseMatcher = new PoseMatcher();
            this.EnterPoseRate = DEFAULT_ENTER_POSE_RATE;
            this.BufferLength = DEFAULT_BUFFER_LENGTH;
            this.Reset();

        }

        public void Start() {
            if (this.State != FormTrainingState.Running)
            {
                this.OnStart(this, new EventArgs());
                this.OnProceed(this, new PoseEventArgs(PoseType.Unknown));
                if (this.State == FormTrainingState.Finished)
                {
                    this.Reset();
                }
                this.NextPose++;
                this.State = FormTrainingState.Running;
            }
        }

        public void Pause() {
            if (this.State == FormTrainingState.Running)
            {
                this.OnPause(this, new EventArgs());
                this.State = FormTrainingState.Paused;
            }
        }

        public void Reset()
        {
            this.State = FormTrainingState.Paused;
            this.NextPose = -1;
            this.bodyBuffer.Clear();
            this.buffer.Clear();
            this.LastPose = PoseType.Unknown;
        }

        public FormTrainingState State { get; set; }

        public static IDictionary<PowerForm, IList<PoseType>> SEQUENCES =
            new ReadOnlyDictionary<PowerForm, IList<PoseType>>(new Dictionary<PowerForm, IList<PoseType>>(){
                {PowerForm.FormOne, new List<PoseType>(){
                    PoseType.DungThang,
                    PoseType.VanVo,
                    PoseType.LapTan,
                    PoseType.TuBinh,
                    PoseType.HoTraoTrai,
                    PoseType.ThoiSonPhai,
                    PoseType.HoiChoPhai,
                    PoseType.TramKieuPhai,
                    PoseType.HoTraoPhai,
                    PoseType.ThoiSonTrai,
                    PoseType.HoiChoTrai,
                    PoseType.TramKieuTrai,
                    PoseType.QuaiChuyTrai,
                    PoseType.TuBinh,
                    PoseType.DungThang
                }.AsReadOnly()}
            });

        public PowerForm Name { get; set; }

        public IList<PoseType> PoseSequence
        {
            get
            {
                return SEQUENCES[this.Name];
            }
        }

        public int NextPose { get; set; }

        /// <summary>
        /// Buffer length
        /// </summary>
        public int BufferLength
        {
            get { return _bufferLength; }
            set
            {
                _bufferLength = value;
                this.bodyBuffer = new FixedSizedQueue<SimpleBody>(_bufferLength);
                this.buffer = new FixedSizedQueue<PoseType>(_bufferLength);

                ///TODO: copy old values over
            }
        }

        /// <summary>
        /// Minimum rate to match a pose (entering a matching state)
        /// </summary>
        public double EnterPoseRate { get; set; }

        public FixedSizedQueue<PoseType> _temporalPoseData = new FixedSizedQueue<PoseType>(TEMPORAL_DATA_LENGTH);

        /// <summary>
        /// Log all the poses matched by timestamp for debuging purposes
        /// </summary>
        public IList<PoseType> TemporalPoseData
        {
            get { return _temporalPoseData.ToList().AsReadOnly(); }
        }

        public PoseType LastPose { get; private set; }

        private void CheckPose()
        {
            if (this.State != FormTrainingState.Running) return;
            if (bodyBuffer.Count < _bufferLength) return;
            PoseType likelyPose = this.buffer.GroupBy(x => x).OrderByDescending(x => x.Count()).Select(x => x.Key).FirstOrDefault();
            int poseCount = this.buffer.Where(x => x == likelyPose).Count();
            if ((double)poseCount / this.BufferLength >= this.EnterPoseRate)
            {
                if (likelyPose != this.LastPose)
                {
                    var eventArgs = new PoseEventArgs(likelyPose);
                    this.OnMatch(this, eventArgs);
                    if (likelyPose == this.PoseSequence[this.NextPose])
                    {
                        if (this.NextPose < this.PoseSequence.Count - 1)
                        {
                            this.OnProceed(this, eventArgs);
                            this.NextPose++;
                        }
                        else
                        {
                            this.OnFinish(this,eventArgs);
                            this.State = FormTrainingState.Finished;
                        }
                    }
                    this.LastPose = likelyPose;
                }
            }
        }
        /// <summary>
        /// Add a new body to the buffer
        /// </summary>
        /// <param name="body">Body to add</param>
        public void AddBody(SimpleBody body)
        {
            this.bodyBuffer.Enqueue(body);
            var pose = this.poseMatcher.Match(body);
            this.buffer.Enqueue(pose);
            _temporalPoseData.Enqueue(pose);
            CheckPose();
        }



        public void Stop()
        {
            this.OnStop(this, new EventArgs());
            this.Reset();
        }
    }
}
