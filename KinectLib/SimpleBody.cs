﻿using Microsoft.Kinect;
using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using OpenTK;
using Vector4 = Microsoft.Kinect.Vector4;

namespace KinectLib
{

    /// <summary>
    /// Class to hold just enough information for the BVH to process
    /// </summary>
    [Serializable]
    [DataContract(Namespace = "http://schemas.datacontract.org/2004/07/KinectLib")]
    public class SimpleBody
    {
        [DataMember]
        public Vector4 FloorClipPlane
        {
            get;
            set;
        }

        public static readonly JointType[] selectedJoints = new JointType[] {
            JointType.Neck,
            JointType.Head,
            JointType.ShoulderLeft,
            JointType.ElbowLeft,
            JointType.WristLeft,
            JointType.HandLeft,
            JointType.HipLeft,
            JointType.KneeLeft,
            JointType.AnkleLeft,
            JointType.ShoulderRight,
            JointType.ElbowRight,
            JointType.WristRight,
            JointType.HandRight,
            JointType.HipRight,
            JointType.KneeRight,
            JointType.AnkleRight
        };

        public static readonly Dictionary<JointType, List<JointType>> JointRelationships = new Dictionary<JointType, List<JointType>>()
        {
            {JointType.SpineBase,new List<JointType>(){JointType.SpineMid,JointType.HipLeft,JointType.HipRight}},
            {JointType.SpineMid,new List<JointType>(){JointType.SpineShoulder}},
            {JointType.SpineShoulder,new List<JointType>(){JointType.Neck, JointType.ShoulderLeft,JointType.ShoulderRight}},
            {JointType.Neck,new List<JointType>(){JointType.Head}},
            {JointType.ShoulderLeft,new List<JointType>(){JointType.ElbowLeft}},
            {JointType.ElbowLeft,new List<JointType>(){JointType.WristLeft}},
            {JointType.WristLeft,new List<JointType>(){JointType.HandLeft}},
            {JointType.HandLeft,new List<JointType>(){JointType.HandTipLeft,JointType.ThumbLeft}},
            {JointType.ShoulderRight,new List<JointType>(){JointType.ElbowRight}},
            {JointType.ElbowRight,new List<JointType>(){JointType.WristRight}},
            {JointType.WristRight,new List<JointType>(){JointType.HandRight}},
            {JointType.HandRight,new List<JointType>(){JointType.HandTipRight,JointType.ThumbRight}},
            {JointType.HipLeft,new List<JointType>(){JointType.KneeLeft}},
            {JointType.KneeLeft,new List<JointType>(){JointType.AnkleLeft}},
            {JointType.AnkleLeft,new List<JointType>(){JointType.FootLeft}},
            {JointType.HipRight,new List<JointType>(){JointType.KneeRight}},
            {JointType.KneeRight,new List<JointType>(){JointType.AnkleRight}},
            {JointType.AnkleRight,new List<JointType>(){JointType.FootRight}}
        };

        public static JointType ROOT_JOINT = JointType.Neck;

        [DataMember]
        public Dictionary<JointType, JointOrientation> JointOrientations { get; set; }

        [DataMember]
        public Dictionary<JointType, Joint> Joints { get; set; }

        public static readonly int N = selectedJoints.Length * 2;


        /// <summary>
        /// Calculate average joint orientations and positions and floor plane
        /// </summary>
        /// <param name="bodyList">IEnumerable of SimpleBody</param>
        /// <returns>The average body</returns>
        public static SimpleBody Average(IEnumerable<SimpleBody> bodyList)
        {
            var retVal = new SimpleBody();
            retVal.JointOrientations = new Dictionary<JointType, JointOrientation>();
            retVal.Joints = new Dictionary<JointType, Joint>();
            foreach (JointType jointType in Enum.GetValues(typeof(JointType)))
            {
                // Average out the  orientations of joint type in interest
                IEnumerable<Quaternion> qList = bodyList.SelectMany(x => x.JointOrientations).Where(x => x.Key == jointType)
                    .Select(x => new Quaternion(
                        x.Value.Orientation.X,
                        x.Value.Orientation.Y,
                        x.Value.Orientation.Z,
                        x.Value.Orientation.W
                     )
                );
                var q = Extensions.MeanQuaternion(qList);
                var o = new JointOrientation();
                o.JointType = jointType;
                o.Orientation = new Microsoft.Kinect.Vector4()
                {
                    X = q.X,
                    Y = q.Y,
                    Z = q.Z,
                    W = q.Z
                };
                retVal.JointOrientations[jointType] = o;

                // Average out joint position
                IEnumerable<Vector3> pList = bodyList.SelectMany(x => x.Joints).Where(x => x.Key == jointType)
                    .Select(x => new Vector3(
                        x.Value.Position.X,
                        x.Value.Position.Y,
                        x.Value.Position.Z
                     )
                );
                var j = new Joint();
                j.JointType = jointType;
                j.Position = new CameraSpacePoint()
                {
                    X = pList.Sum(x => x.X) / pList.Count(),
                    Y = pList.Sum(x => x.Y) / pList.Count(),
                    Z = pList.Sum(x => x.Z) / pList.Count()
                };
                retVal.Joints[jointType] = j;
            }


            // Average out floor plane
            IEnumerable<Quaternion> fList = bodyList.Select(x => x.FloorClipPlane)
                .Select(x => new Quaternion(
                    x.X,
                    x.Y,
                    x.Z,
                    x.W
                 )
            );
            var qF = Extensions.MeanQuaternion(fList);
            var f = new Microsoft.Kinect.Vector4()
            {
                X = qF.X,
                Y = qF.Y,
                Z = qF.Z,
                W = qF.Z
            };
            retVal.FloorClipPlane = f;
            return retVal;
        }

        /// <summary>
        /// Get local orientations in quaternion of this Body
        /// </summary>
        /// <returns>A dictionary with key:jointtype, value:orientation</returns>
        /// <remarks>
        /// Biến đổi từ absolute sang relative (Bất biến với translation, rotation và scale) bằng cách chia (nhân nghịch đảo) các quaternion cho nhau. 
        /// Phép chia quaternion a/b = a * conj(b)
        /// </remarks>
        public Dictionary<JointType, Quaternion> GetLocalOrientations()
        {
            var retVal = new Dictionary<JointType, Quaternion>();
            foreach (KeyValuePair<JointType, List<JointType>> entry in JointRelationships)
            {
                var parentQuat = this.JointOrientations[entry.Key].Orientation.ToQuaternion();
                parentQuat.Conjugate();
                foreach (var childJoint in entry.Value)
                {
                    if (! selectedJoints.Contains(childJoint)) continue;
                    var childQuat = this.JointOrientations[childJoint].Orientation.ToQuaternion();
                    var localQuat = childQuat * parentQuat;
                    if ( localQuat.W < 0){
                        localQuat.W = -localQuat.W;
                        localQuat.X = -localQuat.X;
                        localQuat.Y = -localQuat.Y;
                        localQuat.Z = -localQuat.Z;
                    }
                    retVal[childJoint] = localQuat;
                }
            }
            return retVal;
        }
    }
}