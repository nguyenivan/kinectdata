﻿using Microsoft.Speech.AudioFormat;
using Microsoft.Speech.Recognition;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace KinectLib
{
    public enum VoiceCommand
    {
        Start,
        Stop,
        Quit,
        Yes,
        No
    }
    public delegate void VoiceEventHander(Object sender, VoiceEventArgs e);
    public class VoiceEventArgs : EventArgs
    {
        public VoiceEventArgs(VoiceCommand command)
            : base()
        {
            this.Command = command;
        }

        public VoiceCommand Command { get; set; }
    }
    public class VoiceControl : IDisposable
    {
        /// <summary>
        /// Raised when a voice command is matched
        /// </summary>
        public event VoiceEventHander OnMatch = delegate { };
        private SpeechRecognitionEngine speechEngine;

        /// <summary>
        /// Initialize a new instance of class VoiceControl
        /// </summary>
        public VoiceControl()
        {

            RecognizerInfo ri = TryGetKinectRecognizer();

            if (null != ri)
            {
                this.speechEngine = new SpeechRecognitionEngine(ri.Id);

                // Create a grammar from grammar definition XML file.
                using (var memoryStream = new MemoryStream(Encoding.ASCII.GetBytes(Properties.Resources.SpeechGrammar)))
                {
                    var g = new Grammar(memoryStream);
                    this.speechEngine.LoadGrammar(g);
                }

                this.speechEngine.SpeechRecognized += this.SpeechRecognized;
                this.speechEngine.SpeechRecognitionRejected += this.SpeechRejected;

            }
        }

        public void SetInputAutoStream(Stream stream)
        {
            this.speechEngine.RecognizeAsyncStop();
            this.speechEngine.SetInputToAudioStream(
                stream, new SpeechAudioFormatInfo(EncodingFormat.Pcm, 16000, 16, 1, 32000, 2, null));
            this.speechEngine.RecognizeAsync(RecognizeMode.Multiple);
            // For long recognition sessions (a few hours or more), it may be beneficial to turn off adaptation of the acoustic model. 
            // This will prevent recognition accuracy from degrading over time.
            ////speechEngine.UpdateRecognizerSetting("AdaptationOn", 0);
        }

        private void SpeechRejected(object sender, SpeechRecognitionRejectedEventArgs e)
        {
        }
        /// <summary>
        /// Speech utterance confidence below which we treat speech as if it hadn't been heard
        /// </summary>
        public double ConfidenceThreshold = 0.3;

        /// <summary>
        /// Dictionary to map from vocabulary to actual voice commands
        /// </summary>
        private static Dictionary<string, VoiceCommand> commandDict = new Dictionary<string, VoiceCommand>(){
            {"START", VoiceCommand.Start},
            {"STOP", VoiceCommand.Stop},
            {"QUIT", VoiceCommand.Quit},
            {"YES", VoiceCommand.Yes},
            {"NO", VoiceCommand.No}
        };

        private void SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            if (e.Result.Confidence >= ConfidenceThreshold)
            {
                var word = e.Result.Semantics.Value.ToString();
                try{
                    OnMatch(this, new VoiceEventArgs(commandDict[word]));
                }catch (KeyNotFoundException ex){
                    throw new Exception(string.Format("Word {0} is not known", word), ex);
                }
            }
        }

        private RecognizerInfo TryGetKinectRecognizer()
        {
            IEnumerable<RecognizerInfo> recognizers;

            // This is required to catch the case when an expected recognizer is not installed.
            // By default - the x86 Speech Runtime is always expected. 
            try
            {
                recognizers = SpeechRecognitionEngine.InstalledRecognizers();
            }
            catch (COMException)
            {
                return null;
            }

            foreach (RecognizerInfo recognizer in recognizers)
            {
                string value;
                recognizer.AdditionalInfo.TryGetValue("Kinect", out value);
                if ("True".Equals(value, StringComparison.OrdinalIgnoreCase) && "en-US".Equals(recognizer.Culture.Name, StringComparison.OrdinalIgnoreCase))
                {
                    return recognizer;
                }
            }

            return null;
        }

        public void Dispose()
        {
            this.speechEngine.RecognizeAsyncStop();
            //this.speechEngine.Dispose();
        }
    }
}
