﻿#define RECORD_KINECT

using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Kinect;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Threading;
using System.Windows.Media.Animation;
using MouseKeyboardActivityMonitor;
using MouseKeyboardActivityMonitor.WinApi;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Drawing.Imaging;
using System.Windows.Media.Media3D;
using System.Threading;
using System.Runtime.Serialization;
using System.Xml;
using System.Text;
using KinectLib;

namespace KinectRecorder
{
    /// <summary>
    /// Tracking state
    /// </summary>
    public enum TrackState { Idle, Preparing, Tracking };

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region Private members
        /// <summary>
        /// Radius of drawn hand circles
        /// </summary>
        private const double HandSize = 30;

        /// <summary>
        /// Thickness of drawn joint lines
        /// </summary>
        private const double JointThickness = 3;

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as closed
        /// </summary>
        private readonly Brush handClosedBrush = new SolidColorBrush(Color.FromArgb(128, 255, 0, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as opened
        /// </summary>
        private readonly Brush handOpenBrush = new SolidColorBrush(Color.FromArgb(128, 0, 255, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as in lasso (pointer) position
        /// </summary>
        private readonly Brush handLassoBrush = new SolidColorBrush(Color.FromArgb(128, 0, 0, 255));

        /// <summary>
        /// Size of the RGB pixel in the bitmap
        /// </summary>
        private readonly int bytesPerPixel = (PixelFormats.Bgr32.BitsPerPixel + 7) / 8;

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor kinectSensor = null;

        /// <summary>
        /// Reader for body
        /// frames
        /// </summary>
        private ColorFrameReader colorFrameReader = null;

        /// <summary>
        /// Reader for body frames
        /// </summary>
        private BodyFrameReader bodyFrameReader = null;

        /// <summary>
        /// Width of display (depth space)
        /// </summary>
        private int displayWidth;

        /// <summary>
        /// Height of display (depth space)
        /// </summary>
        private int displayHeight;

        /// <summary>
        /// Array for the bodies
        /// </summary>
        private Body[] bodies = null;

        /// <summary>
        /// Drawing group for body rendering output
        /// </summary>
        private DrawingGroup drawingGroup;

        /// <summary>
        /// Drawing image that we will display
        /// </summary>
        private DrawingImage imageSource;

        /// <summary>
        /// Coordinate mapper to map one type of point to another
        /// </summary>
        private CoordinateMapper coordinateMapper = null;

        /// <summary>
        /// Pen used for drawing bones that are currently inferred
        /// </summary>        
        private readonly Pen inferredBonePen = new Pen(Brushes.Gray, 1);

        /// <summary>
        /// Pen used for drawing bones that are currently tracked
        /// </summary>
        private readonly Pen trackedBonePen = new Pen(Brushes.Green, 6);

        /// <summary>
        /// Brush used for drawing joints that are currently tracked
        /// </summary>
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));

        /// <summary>
        /// Brush used for drawing joints that are currently inferred
        /// </summary>        
        private readonly Brush inferredJointBrush = Brushes.Yellow;

        /// <summary>
        /// Timer to record data at specific interval
        /// </summary>
        private readonly DispatcherTimer timer = new DispatcherTimer();
        private int tickCount = 0;

        /// <summary>
        /// Fake timer to test with redLED bitmap instead of color frames
        /// </summary>
        private readonly DispatcherTimer fakeTimer = new DispatcherTimer();

        /// <summary>
        /// Track state
        /// </summary>
        private TrackState trackState = TrackState.Idle;

        /// <summary>
        /// Flash animation when recording data
        /// </summary>
        private readonly DoubleAnimation flashAnim = new DoubleAnimation
        {
            From = 1.0,
            To = 0.0,
            Duration = new Duration(TimeSpan.FromMilliseconds(100)),
            AutoReverse = true
        };

        /// <summary>
        /// Quick flash for LED in preparing state
        /// </summary>
        private readonly DoubleAnimation quickFlash = new DoubleAnimation
        {
            From = 1.0,
            To = 0.0,
            Duration = new Duration(TimeSpan.FromMilliseconds(50)),
            AutoReverse = true
        };

        /// <summary>
        /// Initiate bitmaps for red and green leds
        /// </summary>
        private readonly BitmapImage greenLED;
        private readonly BitmapImage redLED;

        private Image[] leds = null;

        /// <summary>
        /// Storyboard for KinectCanvas and LEDs
        /// </summary>
        private readonly Storyboard storyBoard = new Storyboard();
        private readonly Storyboard ledStoryBoard = new Storyboard();

        /// <summary>
        /// Global mouse event listener
        /// </summary>
        private readonly MouseHookListener mouseHookManager = new MouseHookListener(new GlobalHooker()) { Enabled = true };

        /// <summary>
        /// Default path: current path
        /// </summary>
        private readonly string defaultPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        /// <summary>
        /// Background worker to create data package
        /// </summary>
        private readonly BackgroundWorker packageBackgroundWorkder = new BackgroundWorker { WorkerSupportsCancellation = true };

        private readonly BackgroundWorker recordBackgroundWorker = new BackgroundWorker { WorkerSupportsCancellation = true };

        private WriteableBitmap realTimeImage = null;

        /// <summary>
        /// Time window in milliseconds
        /// </summary>
        private readonly TimeSpan captureTimeWindow = TimeSpan.FromMilliseconds(500);

        #endregion

        /// <summary>
        /// Queue for data frames, must be synchronized between backgroundworker and UI thread
        /// </summary>
        private readonly Queue<SimpleBody> dataQueue = new Queue<SimpleBody>();
        private readonly Queue<BitmapImage> imageQueue = new Queue<BitmapImage>();
        public WriteableBitmap RealTimeImage
        {
            get { return realTimeImage; }
            set
            {
                realTimeImage = value;
                NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Get or create and get the package folder in format YYYYmmDD
        /// </summary>
        private Tuple<string, int> GetOrCreatePackage
        {
            get
            {
                string timeStamp = DateTime.Today.ToString("yyyyMMdd");
                string packagePath = Path.Combine(defaultPath, timeStamp);
                if (!Directory.Exists(packagePath))
                {
                    Directory.CreateDirectory(packagePath);
                }
                //Regex reg = new Regex(string.Format(@"^.+{0}$", fileExt));
                Regex reg = new Regex(string.Format(@"^\d+{0}$", colorFileExt));
                var files = Directory.GetFiles(packagePath, string.Format("*{0}", colorFileExt)).Where(
                    path => reg.IsMatch(Path.GetFileName(path)));
                return new Tuple<string, int>(packagePath, files.Count() + 1);
            }
        }

        public MainWindow()
        {
            this.kinectSensor = KinectSensor.GetDefault();
            if (this.kinectSensor != null)
            {
                this.coordinateMapper = this.kinectSensor.CoordinateMapper;
                this.kinectSensor.Open();

                FrameDescription colorFrameDescription = this.kinectSensor.ColorFrameSource.FrameDescription;
                FrameDescription bodyFrameDescription = this.kinectSensor.DepthFrameSource.FrameDescription;
                this.displayWidth = colorFrameDescription.Width;
                this.displayHeight = colorFrameDescription.Height;
                this.bodies = new Body[this.kinectSensor.BodyFrameSource.BodyCount];
                this.colorFrameReader = this.kinectSensor.ColorFrameSource.OpenReader();
                this.bodyFrameReader = this.kinectSensor.BodyFrameSource.OpenReader();
                this.drawingGroup = new DrawingGroup();
                this.imageSource = new DrawingImage(this.drawingGroup);
                this.DataContext = this;
            }

            InitializeComponent();

            Assembly assembly = typeof(KinectLib.Extensions).Assembly;
            this.greenLED = new BitmapImage();
            this.greenLED.BeginInit();
            this.greenLED.StreamSource = assembly.GetManifestResourceStream("KinectLib.Resources.greenled.png");
            this.greenLED.EndInit();
            this.redLED = new BitmapImage();
            this.redLED.BeginInit();
            this.redLED.StreamSource = assembly.GetManifestResourceStream("KinectLib.Resources.redled.png");
            this.redLED.EndInit();

            // Set storyboard for animation
            storyBoard.Children.Add(flashAnim);
            Storyboard.SetTargetName(flashAnim, "KinectCanvas");
            Storyboard.SetTargetProperty(flashAnim, new PropertyPath(Image.OpacityProperty));
            flashAnim.Completed += flashAnim_Completed;

            ledStoryBoard.Children.Add(quickFlash);
            Storyboard.SetTargetName(quickFlash, "LEDBoard");
            Storyboard.SetTargetProperty(quickFlash, new PropertyPath(StackPanel.OpacityProperty));



        }

        void flashAnim_Completed(object sender, EventArgs e)
        {
#if CAPTURE_POSE
            foreach (Image l in this.leds)
            {
                l.Source = redLED;
            }
#endif
        }

        /// <summary>
        /// Prepare the effects and events
        /// </summary>
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.colorFrameReader != null)
            {
                this.colorFrameReader.FrameArrived += this.ColorFrameReaderFrameArrived;
            }

            if (this.bodyFrameReader != null)
            {
                this.bodyFrameReader.FrameArrived += this.BodyFrameReaderFrameArrived;
            }

            // Tick the timer every 1 second
            this.timer.Interval = new TimeSpan(0, 0, 1);
            this.timer.Tick += timer_Tick;

            // Setup the LEDs
            this.leds = (from c in this.LEDBoard.Children.OfType<Image>()
                         where "LED".Equals(c.Tag)
                         select c).ToArray<Image>();
            foreach (Image l in leds)
            {
                l.Source = redLED;
            }
            // Setup mouse hooks
            mouseHookManager.MouseClickExt += mouseHookManager_MouseClickExt;

            // Setup background worker
            this.packageBackgroundWorkder.DoWork += packageBackgroundWorkder_DoWork;
            this.packageBackgroundWorkder.RunWorkerAsync();

#if RECORD_KINECT
            this.recordBackgroundWorker.DoWork += recordBackgroundWorker_DoWork;
#endif
            this.recordBackgroundWorker.RunWorkerAsync();
        }


#if RECORD_KINECT
        private readonly Queue<SimpleBody> streamQueue = new Queue<SimpleBody>();
        private void SerializeBodyList()
        {
            int queueLength = this.streamQueue.Count;
            if (queueLength < 1) return;
            var bodyList = new List<SimpleBody>();
            for (int i = 0; i < queueLength; i++)
            {
                bodyList.Add(this.streamQueue.Dequeue());
                // Record the simple bodies under [maxindex]bodies[count].xml format
            }
            var serializer = new DataContractSerializer(typeof(List<SimpleBody>));

            var package = this.GetOrCreatePackage;
            string bodyPath = Path.Combine(package.Item1,
                    string.Format("{0}bodies{1}.xml", package.Item2, queueLength));
            using (var writer = new FileStream(bodyPath, FileMode.Create))
            {
                serializer.WriteObject(writer, bodyList);
                writer.Flush();
            }

        }


        void recordBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                bool queueIsEmpty = true;
                lock (this.streamQueue)
                {
                    if (this.streamQueue.Count > 300)
                    {
                        queueIsEmpty = false;
                    }
                }
                if (queueIsEmpty)
                {
                    System.Diagnostics.Debug.WriteLine("Queue count: {0}", this.streamQueue.Count);
                    System.Diagnostics.Debug.WriteLine("Pause 2000ms");
                    System.Threading.Thread.Sleep(2000);
                }
                else
                {
                    SerializeBodyList();
                }

            }
        }

#endif

        private void SerializePoseList()
        {
            int queueLength = this.dataQueue.Count;
            if (queueLength < 1) return;
            var bodyList = new List<SimpleBody>();
            for (int i = 0; i < queueLength; i++)
            {
                bodyList.Add(this.dataQueue.Dequeue());
                // Record the simple bodies under [maxindex]bodies[count].xml format
            }
            var serializer = new DataContractSerializer(typeof(List<SimpleBody>));

            var package = this.GetOrCreatePackage;
            string bodyPath = Path.Combine(package.Item1,
                    string.Format("{0}poses{1}.xml", package.Item2, queueLength));
            using (var writer = new FileStream(bodyPath, FileMode.Create))
            {
                serializer.WriteObject(writer, bodyList);
                writer.Flush();
            }
        }


        /// <summary>
        /// Save snapshot of color stream and update joint coordinates to the data file
        /// </summary>
        void packageBackgroundWorkder_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                bool queueIsEmpty;
                BitmapImage tempImage = null;
                // Acquire a lock on dataQueue and dequeue quickly
                lock (this.imageQueue)
                {
                    queueIsEmpty = imageQueue.Count == 0;
                    if (!queueIsEmpty)
                    {
                        tempImage = this.imageQueue.Dequeue();
                    }
                }
                if (queueIsEmpty)
                {
                    System.Diagnostics.Debug.WriteLine("Pause 2000ms");
                    System.Threading.Thread.Sleep(2000);
                }
                else if (null != tempImage)
                {
                    Tuple<string, int> package = this.GetOrCreatePackage;
                    string bitmapPath = Path.Combine(package.Item1, string.Format("{0}{1}", package.Item2, colorFileExt));
                    JpegBitmapEncoder jpegEncoder = new JpegBitmapEncoder();
                    jpegEncoder.Frames.Add(BitmapFrame.Create(tempImage));
                    using (FileStream fileStream = new FileStream(bitmapPath, FileMode.Create))
                    {
                        jpegEncoder.Save(fileStream);
                    }
                    System.Threading.Thread.Sleep(100);
                }
                if (dataQueue.Count > 300)
                {
                    SerializePoseList();
                }
            }
        }


        /// <summary>
        /// Listen to global mouse events to start/top the data capturing process
        /// </summary>
        /// <param name="e">Event can use to suppress mouse click</param>
        void mouseHookManager_MouseClickExt(object sender, MouseEventExtArgs e)
        {
            switch (this.trackState)
            {
                case TrackState.Idle:
                    this.timer.Start();
                    this.trackState = TrackState.Preparing;
                    break;
                case TrackState.Preparing:
                case TrackState.Tracking:
                    this.timer.Stop();
                    this.tickCount = 0;
                    foreach (Image l in this.leds)
                    {
                        l.Source = redLED;
                    }
                    this.trackState = TrackState.Idle;
                    break;
            }
        }

        /// <summary>
        /// Excute every tick of the timer dispatcher
        /// </summary>
        private void timer_Tick(object sender, EventArgs e)
        {
            switch (this.trackState)
            {
                case TrackState.Idle:
                    break;
                case TrackState.Preparing:
                    ledStoryBoard.Begin(this, true);
                    subTickCount++;
                    if (this.subTickCount >= this.prepTime)
                    {
                        this.trackState = TrackState.Tracking;
                    }
                    break;
                default:
                    this.tickCount++;
#if CAPTURE_POSE
		            var ledIndex = (this.tickCount - 1) % leds.Length;
                    if (ledIndex == leds.Length - 1)
                    {
                        storyBoard.Begin(this);
                        this.lastTick = DateTime.Now;
                        Extensions.Beep();
                    }
                    this.leds[ledIndex].Source = greenLED;  
#else
                    if (tickCount <= leds.Length)
                    {
                        this.leds[tickCount - 1].Source = greenLED;
                    }
                    if (tickCount == leds.Length)
                    {
                        Extensions.Beep();
                        storyBoard.Begin(this);
                    }

#endif
                    break;
            }
        }

        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        private int subTickCount = 0;
        private int prepTime = 2;
        private DateTime lastBeep;
        private string colorFileExt = ".jpg";
        private DateTime lastTick;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource ImageSource
        {
            get
            {
                return this.imageSource;
            }
        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (this.colorFrameReader != null)
            {
                // ColorFrameReader is IDisposable
                this.colorFrameReader.Dispose();
                this.colorFrameReader = null;

                // BodyFrameReader is IDisposable
                this.bodyFrameReader.Dispose();
                this.bodyFrameReader = null;
            }

            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }

            this.packageBackgroundWorkder.Dispose();
            SerializePoseList();
#if RECORD_KINECT
            SerializeBodyList();
#endif
        }


        private void ColorFrameReaderFrameArrived(object sender, ColorFrameArrivedEventArgs e)
        {
            ColorFrameReference frameReference = e.FrameReference;

            try
            {
                ColorFrame frame = frameReference.AcquireFrame();

                if (frame != null)
                {
                    // ColorFrame is IDisposable
                    using (frame)
                    {
                        FrameDescription frameDescription = frame.CreateFrameDescription(ColorImageFormat.Bgra);
                        int width = frameDescription.Width;
                        int height = frameDescription.Height;
                        uint bufferLength = (uint)(frameDescription.LengthInPixels * frameDescription.BytesPerPixel);

                        if (null == realTimeImage)
                        {
                            realTimeImage = new WriteableBitmap(width, height, 96, 96, PixelFormats.Bgra32, null);
                        }
                        realTimeImage.Lock();
                        frame.CopyConvertedFrameDataToIntPtr(realTimeImage.BackBuffer,bufferLength, ColorImageFormat.Bgra);
                        realTimeImage.AddDirtyRect(new Int32Rect(0, 0, width, height));
                        realTimeImage.Unlock();
                        NotifyPropertyChanged("RealTimeImage");

                    }
                }
            }
            catch (Exception)
            {
                throw;
                // ignore if the frame is no longer available
            }
        }


        private void BodyFrameReaderFrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            BodyFrameReference frameReference = e.FrameReference;
            // If nothing is tracked, null the joints reference
            try
            {
                BodyFrame frame = frameReference.AcquireFrame();

                if (frame != null)
                {
                    // BodyFrame is IDisposable
                    using (frame)
                    {
                        using (DrawingContext dc = this.drawingGroup.Open())
                        {
                            // Draw a transparent background to set the render size

                            dc.DrawRectangle(Brushes.Transparent, null, new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));

                            // draw the kinect bitmap if it's there
                            if (null != RealTimeImage)
                            {
                                // determine the coordinates for displaying the image
                                Double w = realTimeImage.Width * this.displayHeight / realTimeImage.Height;
                                Double diffWidth = Math.Abs(this.displayWidth - w);
                                Double x = -(diffWidth / 2);
                                Double ww = w + x;
                                dc.DrawImage(realTimeImage, new Rect(x, 0.0, w, this.displayHeight));
                            }

                            // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                            // As long as those body objects are not disposed and not set to null in the array,
                            // those body objects will be re-used.
                            frame.GetAndRefreshBodyData(this.bodies);

                            foreach (Body body in this.bodies)
                            {
                                if (body.IsTracked)
                                {
                                    IReadOnlyDictionary<JointType, Joint> joints = body.Joints;

                                    if (null != joints)
                                    {
                                        if (this.trackState == TrackState.Tracking)
                                        {
                                            // Create double array from joint set, we need to create a deep copy first
                                            var jointsCopy = joints.ToDictionary(
                                                entry => entry.Key, entry => entry.Value);
                                            var jointOrienationsCopy = body.JointOrientations.ToDictionary(
                                                entry => entry.Key, entry => entry.Value);
                                            var simpleBody = new SimpleBody()
                                            {
                                                Joints = jointsCopy,
                                                JointOrientations = jointOrienationsCopy,
                                                FloorClipPlane = frame.FloorClipPlane
                                            };

                                            // Record pose if still in capture time window
                                            DateTime currentTime = DateTime.Now;
#if CAPTURE_POSE
                                            if (currentTime - lastTick < captureTimeWindow)
                                            {

                                                BitmapImage tempBitmap = new BitmapImage();

                                                // Copy data to a bitmap
                                                // Create BitmapImage and use JpegDecoder to save to file
                                                using (MemoryStream memoryStream = new MemoryStream())
                                                {
                                                    JpegBitmapEncoder jpegEncoder = new JpegBitmapEncoder();
                                                    jpegEncoder.Frames.Add(BitmapFrame.Create(realTimeImage));
                                                    jpegEncoder.Save(memoryStream);
                                                    memoryStream.Seek(0, SeekOrigin.Begin);
                                                    tempBitmap.BeginInit();
                                                    tempBitmap.CacheOption = BitmapCacheOption.OnLoad;
                                                    tempBitmap.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                                                    tempBitmap.StreamSource = memoryStream;
                                                    tempBitmap.EndInit();
                                                }
                                                this.imageQueue.Enqueue(tempBitmap);
                                                this.dataQueue.Enqueue(simpleBody);
                                            }
#endif

#if RECORD_KINECT
                                                this.streamQueue.Enqueue(simpleBody);

#endif
                                        }
                                        // convert the joint points to depth (display) space
                                        Dictionary<JointType, Point> jointPoints = new Dictionary<JointType, Point>();
                                        foreach (JointType jointType in joints.Keys)
                                        {
                                            ColorSpacePoint colorSpacePoint = this.coordinateMapper.MapCameraPointToColorSpace(joints[jointType].Position);
                                            jointPoints[jointType] = new Point(colorSpacePoint.X, colorSpacePoint.Y);
                                        }
                                        this.DrawBody(joints, jointPoints, dc);
                                        this.DrawHand(body.HandLeftState, jointPoints[JointType.HandLeft], dc);
                                        this.DrawHand(body.HandRightState, jointPoints[JointType.HandRight], dc);

                                    }
                                }
                            }
                            // prevent drawing outside of our render area
                            this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
                // ignore if the frame is no longer available
            }
        }

        #region Canvas Drawing Methods
        /// <summary>
        /// Draws a hand symbol if the hand is tracked: red circle = closed, green circle = opened; blue circle = lasso
        /// </summary>
        /// <param name="handState">state of the hand</param>
        /// <param name="handPosition">position of the hand</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawHand(HandState handState, Point handPosition, DrawingContext drawingContext)
        {
            switch (handState)
            {
                case HandState.Closed:
                    drawingContext.DrawEllipse(this.handClosedBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Open:
                    drawingContext.DrawEllipse(this.handOpenBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Lasso:
                    drawingContext.DrawEllipse(this.handLassoBrush, null, handPosition, HandSize, HandSize);


                    break;
            }
        }

        /// <summary>
        /// Draws one bone of a body (joint to joint)
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="jointType0">first joint of bone to draw</param>
        /// <param name="jointType1">second joint of bone to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawBone(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, Point> jointPoints, JointType jointType0, JointType jointType1, DrawingContext drawingContext)
        {
            Joint joint0 = joints[jointType0];
            Joint joint1 = joints[jointType1];

            // If we can't find either of these joints, exit
            if (joint0.TrackingState == TrackingState.NotTracked ||
                joint1.TrackingState == TrackingState.NotTracked)
            {
                return;
            }

            // Don't draw if both points are inferred
            if (joint0.TrackingState == TrackingState.Inferred &&
                joint1.TrackingState == TrackingState.Inferred)
            {
                return;
            }

            // We assume all drawn bones are inferred unless BOTH joints are tracked
            Pen drawPen = this.inferredBonePen;
            if ((joint0.TrackingState == TrackingState.Tracked) && (joint1.TrackingState == TrackingState.Tracked))
            {
                drawPen = this.trackedBonePen;
            }

            drawingContext.DrawLine(drawPen, jointPoints[jointType0], jointPoints[jointType1]);
        }

        /// <summary>
        /// Draws a body
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawBody(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, Point> jointPoints, DrawingContext drawingContext)
        {
            // Draw the bones

            // Torso
            this.DrawBone(joints, jointPoints, JointType.Head, JointType.Neck, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.Neck, JointType.SpineShoulder, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.SpineMid, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineMid, JointType.SpineBase, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.ShoulderRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.ShoulderLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineBase, JointType.HipRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineBase, JointType.HipLeft, drawingContext);

            // Right Arm    
            this.DrawBone(joints, jointPoints, JointType.ShoulderRight, JointType.ElbowRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.ElbowRight, JointType.WristRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristRight, JointType.HandRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.HandRight, JointType.HandTipRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristRight, JointType.ThumbRight, drawingContext);

            // Left Arm
            this.DrawBone(joints, jointPoints, JointType.ShoulderLeft, JointType.ElbowLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.ElbowLeft, JointType.WristLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristLeft, JointType.HandLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.HandLeft, JointType.HandTipLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristLeft, JointType.ThumbLeft, drawingContext);

            // Right Leg
            this.DrawBone(joints, jointPoints, JointType.HipRight, JointType.KneeRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.KneeRight, JointType.AnkleRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.AnkleRight, JointType.FootRight, drawingContext);

            // Left Leg
            this.DrawBone(joints, jointPoints, JointType.HipLeft, JointType.KneeLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.KneeLeft, JointType.AnkleLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.AnkleLeft, JointType.FootLeft, drawingContext);

            // Draw the joints
            foreach (JointType jointType in joints.Keys)
            {
                Brush drawBrush = null;

                TrackingState trackingState = joints[jointType].TrackingState;

                if (trackingState == TrackingState.Tracked)
                {
                    drawBrush = this.trackedJointBrush;
                }
                else if (trackingState == TrackingState.Inferred)
                {
                    drawBrush = this.inferredJointBrush;
                }

                if (drawBrush != null)
                {
                    drawingContext.DrawEllipse(drawBrush, null, jointPoints[jointType], JointThickness, JointThickness);
                }
            }
        }
        #endregion

    }


}
